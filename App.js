/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import Route from './src/Route';
import GlobalProvider from './src/context/GlobalContext';
import codePush from 'react-native-code-push';

const App = () => {
  return (
    <GlobalProvider>
      <Route />
    </GlobalProvider>
  );
};

export default codePush(App);
