import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import colors from './res/colors/index';
import Icon from 'react-native-vector-icons/FontAwesome';

import SplashScreen from './view/splash-screen/SplashScreen';
import MuridPage from './view/dashboard-murid/MuridPage';
import ProfilePage from './view/profile/ProfilePage';
import TugasPage from './view/tugas/TugasPage';
import DetailMateri from './view/materi/DetailMateri';
import LoginPage from './view/login/LoginPage';
import GuruPage from './view/dashboard-guru/GuruPage';
import MateriPage from './view/materi/MateriPage';
import AddMateri from './view/materi/AddMateri';
import AddTugas from './view/tugas/AddTugas';
import DetailTugas from './view/tugas/DetailTugas';
import AddMurid from './view/murid/AddMurid';
import ListMuridPage from './view/murid/ListMuridPage';
import AdminPage from './view/admin/AdminPage';
import AddUser from './view/admin/AddUser';

import MuridMateriPage from './view/murid-materi/MateriPage';
import MuridDetailMateri from './view/murid-materi/DetailMateri';

import InputHasilTugas from './view/tugas-murid/InputHasilTugas';
import DetailHasil from './view/tugas/DetailHasil';
import PelajaranPage from './view/admin-pelajaran/PelajaranPage';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const TabNav = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          let iconName;

          if (route.name === 'Dashboard') {
            iconName = 'home';
          } else if (route.name === 'Profile') {
            iconName = 'user';
          } else if (route.name === 'Tugas') {
            iconName = 'tasks';
          }

          // You can return any component that you like here!
          return <Icon name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: colors.blueDefault,
        inactiveTintColor: colors.gray09,
        activeBackgroundColor: 'white',
        inactiveBackgroundColor: 'white',
      }}>
      <Tab.Screen name="Dashboard" component={MuridPage} />
      {/* <Tab.Screen name="Tugas" component={TugasPage} /> */}
      <Tab.Screen name="Profile" component={ProfilePage} />
    </Tab.Navigator>
  );
};

const TabGuru = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          let iconName;

          if (route.name === 'Dashboard') {
            iconName = 'home';
          } else if (route.name === 'Profile') {
            iconName = 'user';
          } else if (route.name === 'Tugas') {
            iconName = 'tasks';
          }

          // You can return any component that you like here!
          return <Icon name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: colors.blueDefault,
        inactiveTintColor: colors.gray09,
        activeBackgroundColor: 'white',
        inactiveBackgroundColor: 'white',
      }}>
      <Tab.Screen name="Dashboard" component={GuruPage} />
      {/* <Tab.Screen name="Tugas" component={TugasPage} /> */}
      <Tab.Screen name="Profile" component={ProfilePage} />
    </Tab.Navigator>
  );
};

const Route = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="LoginPage"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="LoginPage" component={LoginPage} />
        <Stack.Screen name="TabNav" component={TabNav} />
        <Stack.Screen name="TabGuru" component={TabGuru} />
        <Stack.Screen name="MateriPage" component={MateriPage} />
        <Stack.Screen name="DetailMateri" component={DetailMateri} />
        <Stack.Screen name="AddMateri" component={AddMateri} />
        <Stack.Screen name="TugasPage" component={TugasPage} />
        <Stack.Screen name="AddTugas" component={AddTugas} />
        <Stack.Screen name="DetailTugas" component={DetailTugas} />
        <Stack.Screen name="AddMurid" component={AddMurid} />
        <Stack.Screen name="ListMuridPage" component={ListMuridPage} />
        <Stack.Screen name="AdminPage" component={AdminPage} />
        <Stack.Screen name="AddUser" component={AddUser} />
        <Stack.Screen name="MuridMateriPage" component={MuridMateriPage} />
        <Stack.Screen name="MuridDetailMateri" component={MuridDetailMateri} />
        <Stack.Screen name="InputHasilTugas" component={InputHasilTugas} />
        <Stack.Screen name="DetailHasil" component={DetailHasil} />
        <Stack.Screen name="PelajaranPage" component={PelajaranPage} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Route;
