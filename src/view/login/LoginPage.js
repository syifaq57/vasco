/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  Keyboard,
  AsyncStorage,
} from 'react-native';
import {Container} from 'native-base';
import firestore from '@react-native-firebase/firestore';
import colors from '../../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/native';
import ReTextInput from '../../component/ReTextInput';
import {useGlobalContext} from '../../context/GlobalContext';

import ReLoadingLocal from '../../component/ReLoadingLocal';

const LoginPage = () => {
  const initialState = {
    displayTitle: true,
    isLoading: false,
    localLoad: false,
  };

  const initialForm = {
    username: '',
    password: '',
  };
  const [formData, setFormData] = useState(initialForm);
  const [state, setState] = useState(initialState);
  const navigation = useNavigation();
  const onPressLogin = () => {
    // navigation.navigate('TabNav');
    LoginUser();
  };

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const updateForm = (newData) => {
    setFormData((prev) => ({...prev, ...newData}));
  };

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', onKeyboardDidShow);
    Keyboard.addListener('keyboardDidHide', onKeyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', onKeyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', onKeyboardDidHide);
    };
  }, []);

  const LoginUser = async () => {
    await updateState({isLoading: true});
    await firestore()
      .collection('user')
      .where('username', '==', formData.username)
      .where('password', '==', formData.password)
      .onSnapshot((snapshot) => {
        let User = [];

        snapshot.forEach((doc) => {
          User.push({
            id: doc.id,
            nama: doc.data().nama,
            kelas: doc.data().kelas,
            username: doc.data().username,
            password: doc.data().password,
            foto: doc.data().foto,
            role: doc.data().role,
          });
        });

        if (User.length > 0) {
          AsyncStorage.setItem('userData', JSON.stringify(User[0]));
          if (User[0].role === 'murid') {
            navigation.navigate('TabNav');
          } else {
            navigation.navigate('TabGuru');
          }
        } else {
          alert(' Username / Password tidak sesuai');
        }
      });

    updateState({isLoading: false});
  };

  const checkUserData = async () => {
    await updateState({localLoad: true});
    await AsyncStorage.getItem('userData').then((value) => {
      const data = JSON.parse(value);
      if (data !== null) {
        updateForm({
          username: data.username,
          password: data.password,
        });
      }
    });
    await updateState({localLoad: false});
  };

  useEffect(() => {
    checkUserData();
  }, []);

  const onKeyboardDidShow = () => {
    updateState({displayTitle: false});
  };

  const onKeyboardDidHide = () => {
    updateState({displayTitle: true});
  };

  return (
    <Container style={{flex: 1}}>
      <ImageBackground
        // source={require('../../res/images/bg2.png')}
        style={{
          flex: 1,
          position: 'absolute',
          width: '100%',
          height: '100%',
          resizeMode: 'cover',
          // justifyContent: 'center',
          backgroundColor: colors.blueDefault,
        }}>
        {/* Content */}
        <View
          // behavior="padding"
          // enabled
          // keyboardVerticalOffset={0}
          style={{flex: 1, paddingHorizontal: wp(4), paddingVertical: hp(2)}}>
          {state.displayTitle ? (
            <View
              style={{
                paddingHorizontal: wp(2),
                flex: 1,
                alignItems: 'center',
                marginTop: hp(5),
              }}>
              <Image
                style={{
                  // marginTop: hp(2),
                  height: hp(15),
                  // borderRadius: wp(50),
                  width: wp(20),
                  resizeMode: 'contain',
                  borderWidth: 1,
                }}
                source={require('../../res/images/school.png')}
              />
              <View style={{marginTop: hp(2)}}>
                <Text
                  style={{
                    color: colors.white,
                    fontSize: wp(7),
                    fontFamily: 'Poppins-Bold',
                  }}>
                  Vasco Study
                </Text>
              </View>
            </View>
          ) : null}

          {/* Login Form */}
          {state.localLoad ? (
            <View style={{paddingVertical: hp('2%')}}>
              <ActivityIndicator size={'large'} />
            </View>
          ) : (
            <View
              style={{
                paddingHorizontal: wp(2),
                flex: !state.displayTitle ? 1 : 2,
                justifyContent: 'center',
              }}>
              <View style={{marginVertical: hp(2)}}>
                <ReTextInput
                  value={formData.username}
                  // passwordInput
                  label="Username"
                  loginInput
                  onChangeText={(text) => {
                    updateForm({username: text});
                  }}
                />
              </View>
              <View style={{marginVertical: hp(2)}}>
                <ReTextInput
                  value={formData.password}
                  passwordInput
                  loginInput
                  label="Password"
                  onChangeText={(text) => {
                    updateForm({password: text});
                  }}
                />
              </View>
              <View style={{marginVertical: hp(4)}}>
                <TouchableOpacity
                  style={{
                    backgroundColor: colors.soft1,
                    borderRadius: wp(1.5),
                    height: hp(7.6),
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  onPress={() => onPressLogin()}>
                  <Text
                    style={{
                      fontSize: wp(4.5),
                      color: colors.white,
                      // fontWeight: 'bold',
                      fontFamily: 'JosefinSans-Bold',
                    }}>
                    LOGIN
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </View>
        <ReLoadingLocal visible={state.isLoading} title="Authentication" />
      </ImageBackground>
    </Container>
    // <Container
    //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
    //   <View>
    //     <Text style={{fontSize: wp(5)}}>APP Disabled</Text>
    //     <Text style={{fontSize: wp(5)}}>Contact Developer</Text>
    //   </View>
    // </Container>
  );
};

export default LoginPage;
