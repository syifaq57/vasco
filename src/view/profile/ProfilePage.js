/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  AsyncStorage,
  ActivityIndicator,
} from 'react-native';
import {Container, Header, Card, Content} from 'native-base';
import colors from '../../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import DefaultHeader from '../../component/DefaultHeader';
import {useGlobalContext} from '../../context/GlobalContext';
import ReButtonSave from '../../component/ReButtonSave';
import ReTextInput from '../../component/ReTextInput';
import {useNavigation} from '@react-navigation/native';
import Api from '../../services/Api';
import {useIsFocused} from '@react-navigation/native';

const ProfilePage = () => {
  const {getGlobalState, setGlobalState, setLoading} = useGlobalContext();
  const [isLoading, setIsLoading] = useState(false);
  const [visibleProfile, setVisibleProfile] = useState(false);
  const {updateUser} = Api();
  const isFocused = useIsFocused();
  const [formData, setFormData] = useState({
    username: '',
    password: '',
    nama: '',
  });

  const navigation = useNavigation();

  const updateFormData = (newData) => {
    setFormData((prev) => ({...prev, ...newData}));
  };

  useEffect(() => {
    UpdateForm();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [visibleProfile, isFocused]);

  const UpdateForm = () => {
    updateFormData({
      username: getGlobalState('userData').username,
      password: getGlobalState('userData').password,
      nama: getGlobalState('userData').nama,
      id: getGlobalState('userData').id,
      kelas: getGlobalState('userData').kelas,
      foto: getGlobalState('userData').foto,
      role: getGlobalState('userData').role,
      imageObject: {},
    });
  };

  const onSaveProfile = async () => {
    await setIsLoading(true);
    const solved = await updateUser(formData);
    if (solved === true) {
      navigation.navigate('LoginPage');
      alert('Berhasil Mengubah Profile');
    } else {
      alert('Gagal Menympan Materi');
    }
    await setIsLoading(false);

  };

  return (
    <Container style={{flex: 1}}>
      <ImageBackground
        source={require('../../res/images/bg2.png')}
        style={{
          flex: 1,
          resizeMode: 'cover',
          // justifyContent: 'center',
        }}>
        <DefaultHeader
          title="Profile"
          rightButton="pencil"
          onPressButtonRight={() => setVisibleProfile(!visibleProfile)}
        />
        {isLoading ? (
          <View style={{paddingVertical: hp('2%')}}>
            <ActivityIndicator size={'large'} />
          </View>
        ) : (
          <Content style={{}}>
            <View
              style={{
                // marginTop: hp(8.5),
                backgroundColor: colors.blueDefault,
                // borderRadius: hp(1),
                minHeight: hp(20),
                paddingBottom: hp(2),
              }}>
              <View
                style={{
                  top: hp(1.5),
                  alignSelf: 'center',
                }}>
                <Image
                  style={{
                    width: wp(25),
                    height: wp(25),
                    borderRadius: wp(25),
                  }}
                  source={{uri: getGlobalState('userData').foto}}
                  resizeMode="cover"
                />
              </View>
              {/* <View
              style={{
                alignItems: 'flex-end',
                paddingRight: wp(2),
                paddingTop: hp(1),
              }}>
              <TouchableOpacity
                style={{padding: wp(2), borderRadius: wp(1), backgroundColor: colors.soft1}}>
                <Text>Edit Profile</Text>
              </TouchableOpacity>
            </View> */}
              <View
                style={{
                  marginTop: hp(2),
                  marginBottom: hp(1),
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    flex: 2,
                    color: 'white',
                    fontSize: wp(5),
                    fontFamily: 'JosefinSans-Bold',
                  }}>
                  {getGlobalState('userData').nama}
                </Text>
                <Text
                  style={{
                    flex: 2,
                    color: 'white',
                    fontSize: wp(5),
                    fontFamily: 'JosefinSans-Bold',
                  }}>
                  {getGlobalState('userData').role === 'Admin'
                    ? getGlobalState('userData').role
                    : getGlobalState('userData').role === 'Guru'
                    ? getGlobalState('userData').role
                    : getGlobalState('userData').kelas}
                </Text>
              </View>
            </View>
            {visibleProfile ? (
              <View style={{marginVertical: hp(2), paddingHorizontal: wp(6)}}>
                <View style={{marginLeft: wp(2)}}>
                  <Text
                    style={{
                      flex: 2,
                      color: colors.gray08,
                      fontSize: wp(5),
                      fontFamily: 'JosefinSans-Bold',
                    }}>
                    Edit Profile
                  </Text>
                </View>
                <View style={{marginVertical: hp(1.2)}}>
                  <ReTextInput
                    label="Username"
                    value={formData.username}
                    onChangeText={(text) => {
                      updateFormData({username: text});
                    }}
                  />
                </View>
                <View style={{marginVertical: hp(1.2)}}>
                  <ReTextInput
                    label="Password"
                    value={formData.password}
                    onChangeText={(text) => {
                      updateFormData({password: text});
                    }}
                  />
                </View>
                <View style={{marginVertical: hp(1.2)}}>
                  <ReTextInput
                    label="Nama"
                    value={formData.nama}
                    onChangeText={(text) => {
                      updateFormData({nama: text});
                    }}
                  />
                </View>
                <ReButtonSave
                  title="Simpan Profile"
                  color={colors.soft1}
                  // disabled={checkFilledForm()}
                  onPress={() => {
                    onSaveProfile();
                  }}
                />
              </View>
            ) : (
              <View>
                <View style={{marginVertical: hp(2), marginHorizontal: wp(3)}}>
                  <ReButtonSave
                    title="LOGOUT"
                    color={colors.danger}
                    // disabled={checkFilledForm()}
                    onPress={() => {
                      navigation.navigate('LoginPage');
                    }}
                  />
                </View>
                <View
                  style={{
                    marginVertical: hp(1),
                    flex: 1,
                    alignItems: 'center',
                  }}>
                  <Text>Version : V. 1.3</Text>
                </View>
              </View>
            )}

            <View style={{height: hp(10)}} />
          </Content>
        )}

        {/* Content */}
        <View style={{paddingHorizontal: wp(4)}}>
          <ScrollView />
        </View>
      </ImageBackground>
    </Container>
  );
};

export default ProfilePage;
