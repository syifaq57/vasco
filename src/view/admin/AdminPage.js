/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground,
} from 'react-native';
import {Container, Header, Button, Content} from 'native-base';
import colors from '../../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import DefaultHeader from '../../component/DefaultHeader';
import AdminItem from './AdminItem';
import {useNavigation} from '@react-navigation/native';
import Api from '../../services/Api';

const AdminPage = () => {
  const navigation = useNavigation();
  const {user} = Api();
  const [search, setSearch] = useState('');
  const [listMurid, setMurid] = useState([]);
  const [loadingScreen, setLoadingScreen] = useState(false);

  const loadMateri = async () => {
    await setLoadingScreen(true);
    await user
      .orderBy('createAt', 'desc')
      // .where('role', '==', 'murid')
      .onSnapshot((snapshot) => {
        let Murid = [];

        snapshot.forEach((doc) => {
          Murid.push({
            id: doc.id,
            nama: doc.data().nama,
            kelas: doc.data().kelas,
            username: doc.data().username,
            password: doc.data().password,
            foto: doc.data().foto,
            role: doc.data().role,
          });
        });
        setMurid(Murid);
      });
    await setLoadingScreen(false);
  };

  useEffect(() => {
    const subscriber = loadMateri();
    return () => subscriber;
  }, []);

  return (
    <Container style={{flex: 1}}>
      <ImageBackground
        source={require('../../res/images/bg2.png')}
        style={{
          flex: 1,
          resizeMode: 'cover',
          // justifyContent: 'center',
        }}>
        <DefaultHeader
          title="Daftar User"
          backButton
          rightButton="plus"
          onPressButtonRight={() => navigation.navigate('AddUser')}
          searchable
          onSearchChange={async (val) => {
            await setSearch(val);
          }}
        />

        {/* Content */}
        <Content style={{flex: 1, paddingHorizontal: wp(4)}}>
          <View style={{marginVertical: hp(1)}}>
            {loadingScreen ? (
              <View style={{paddingVertical: hp('2%')}}>
                <ActivityIndicator size={'large'} />
              </View>
            ) : (
              <AdminItem
                data={listMurid.filter((y) => y.nama.includes(search))}
              />
            )}
          </View>
          <View style={{height: hp(15)}} />
        </Content>
      </ImageBackground>
    </Container>
  );
};

export default AdminPage;
