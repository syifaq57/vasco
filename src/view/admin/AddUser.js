/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
} from 'react-native';
import {Container, Content, Button} from 'native-base';
import colors from '../../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DefaultHeader from '../../component/DefaultHeader';
import {useNavigation, useRoute} from '@react-navigation/native';
import ReTextInput from '../../component/ReTextInput';
import RePicker from '../../component/RePicker';
import ReTextArea from '../../component/ReTextArea';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-crop-picker';
import Api from '../../services/Api';
import ReButtonSave from '../../component/ReButtonSave';

const listKelas = [
  {
    value: 'Kelas VI',
    label: 'Kelas VI',
  },
  {
    value: 'Kelas VII',
    label: 'Kelas VII',
  },
  {
    value: 'Kelas VIII',
    label: 'Kelas VIII',
  },
];

const listRole = [
  {
    value: 'Guru',
    label: 'Guru',
  },
  {
    value: 'Murid',
    label: 'Murid',
  },
  {
    value: 'Admin',
    label: 'Admin',
  },
];

const AddUser = () => {
  const initialState = {
    pickerVisible: false,
    imageAviable: false,
    disabledSave: true,
    isOpenImage: false,
  };

  const initialFormData = {
    id: '',
    nama: '',
    kelas: '',
    username: '',
    password: '',
    foto: '',
    role: 'Guru',
    // nama: 'Bambang',
    // judul: 'Matematika aljabar linier',
    // pelajaran: 'Matematika',
    // kelas: 'Kelas VIII',
    // isiMateri: 'isi materi',
    imageObject: [],
  };
  const navigation = useNavigation();
  const route = useRoute();
  const [state, setState] = useState(initialState);
  const [loadingScreen, setLoadingScreen] = useState(false);
  const [formData, setFormData] = useState(initialFormData);
  const {inputUser, updateUser} = Api();

  useEffect(() => {
    checkParams();
  }, []);

  const checkParams = async () => {
    const data = (await route.params) ? route.params.data : null;
    if (data !== null) {
      await setLoadingScreen(true);
      await updateFormData({
        id: data.id,
        nama: data.nama,
        kelas: data.kelas,
        username: data.username,
        password: data.password,
        foto: data.foto,
        role: data.role,
      });

      await setLoadingScreen(false);
    }
  };

  useEffect(() => {
    if (formData.foto.length <= 0) {
      updateState({imageAviable: false});
    } else {
      updateState({imageAviable: true});
    }
  }, [formData.foto]);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const updateFormData = (newData) => {
    setFormData((prev) => ({...prev, ...newData}));
  };

  const pickSingle = (cropping, mediaType = 'photo') => {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(async (image) => {
        await updateFormData({
          imageObject: {
            name: image.modificationDate,
            exif: image.exif,
            uri: image.path,
            width: image.width,
            height: image.height,
            mime: image.mime,
          },
        });
        updateFormData({foto: ''});
        updateState({
          imageAviable: true,
          pickerVisible: false,
        });
      })
      .catch((e) => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  };

  const pickSingleWithCamera = (cropping, mediaType = 'photo') => {
    ImagePicker.openCamera({
      cropping: cropping,
      compressImageQuality: 0.2,
      includeExif: true,
      mediaType,
    })
      .then(async (image) => {
        await updateFormData({
          imageObject: {
            name: image.modificationDate,
            exif: image.exif,
            uri: image.path,
            width: image.width,
            height: image.height,
            mime: image.mime,
          },
        });
        updateFormData({foto: ''});
        updateState({
          imageAviable: true,
          pickerVisible: false,
        });
      })
      .catch((e) => alert(e));
    togglePicker();
  };

  const togglePicker = () => {
    updateState({pickerVisible: !state.pickerVisible});
  };

  const checkFilledForm = () => {
    if (
      formData.nama.length > 0 &&
      // formData.kelas.length > 0 &&
      formData.username.length > 0 &&
      formData.password.length > 0 &&
      formData.role.length > 0
    ) {
      return false;
    }

    return true;
  };

  const handleCreateData = async () => {
    const solved = await inputUser(formData);
    if (solved === true) {
      navigation.goBack();
    } else {
      alert('Gagal Menympan Materi');
    }
  };

  const handleUpdateData = async () => {
    const solved = await updateUser(formData);
    if (solved === true) {
      navigation.goBack();
    } else {
      alert('Gagal Menympan Materi');
    }
  };

  const handleOnSave = async () => {
    if (formData.id.length > 0) {
      handleUpdateData();
    } else {
      handleCreateData();
    }
  };

  return (
    <Container style={{flex: 1}}>
      <ImageBackground
        source={require('../../res/images/bg2.png')}
        style={{
          flex: 1,
          resizeMode: 'cover',
          // justifyContent: 'center',
        }}>
        <DefaultHeader
          title={route.params ? 'Edit User' : 'Tambah User'}
          backButton
          // rightButton="plus"
          // onPressButtonRight={() => navigation.navigate('AddMurid')}
          // searchable
        />

        {/* Content */}
        {loadingScreen ? (
          <View style={{paddingVertical: hp('2%')}}>
            <ActivityIndicator size={'large'} />
          </View>
        ) : (
          <Content style={{paddingHorizontal: wp(4)}}>
            <View style={{marginTop: hp(3), marginBottom: hp(1)}}>
              <Text
                style={{
                  fontFamily: 'Poppins-Regular',
                  fontSize: wp(4),
                  color: 'black',
                }}>
                *Isi Form User
              </Text>
            </View>
            <View style={{marginVertical: hp(1.2)}}>
              <TouchableOpacity
                onPress={() => {
                  togglePicker();
                }}
                style={{justifyContent: 'center', alignItems: 'center'}}
                transparent>
                {state.imageAviable === true ? (
                  <Image
                    source={{
                      uri:
                        formData.foto.length > 0
                          ? formData.foto
                          : formData.imageObject.uri,
                    }}
                    style={{
                      height: hp(13),
                      width: hp(13),
                      borderRadius: hp(20),
                      resizeMode: 'cover',
                    }}
                  />
                ) : (
                  <Image
                    source={require('../../res/images/Profile.png')}
                    style={{
                      height: hp('13%'),
                      width: hp('13%'),
                      borderRadius: hp('20%'),
                      resizeMode: 'cover',
                    }}
                  />
                )}
              </TouchableOpacity>
              <View style={{alignItems: 'center'}}>
                <Text>Tambah Foto Profile</Text>
              </View>
            </View>
            <View style={{marginVertical: hp(1.5)}}>
              <RePicker
                label="Role"
                data={listRole}
                value={formData.role}
                onValueChange={(value) => {
                  updateFormData({role: value});
                }}
              />
            </View>
            <View style={{marginVertical: hp(1.2)}}>
              <ReTextInput
                label="Username"
                value={formData.username}
                onChangeText={(text) => {
                  updateFormData({username: text});
                }}
              />
            </View>
            <View style={{marginVertical: hp(1.2)}}>
              <ReTextInput
                label="Password"
                value={formData.password}
                onChangeText={(text) => {
                  updateFormData({password: text});
                }}
              />
            </View>
            <View style={{marginVertical: hp(1.2)}}>
              <ReTextInput
                label="Nama"
                value={formData.nama}
                onChangeText={(text) => {
                  updateFormData({nama: text});
                }}
              />
            </View>
            {formData.role === 'Murid' ? (
              <View style={{marginVertical: hp(1.5)}}>
                <RePicker
                  label="Kelas"
                  data={listKelas}
                  value={formData.kelas}
                  onValueChange={(value) => {
                    updateFormData({kelas: value});
                  }}
                />
              </View>
            ) : null}

            <View style={{marginVertical: hp(1.5)}}>
              <ReButtonSave
                  disabled={checkFilledForm()}
                onPress={() => {
                  handleOnSave();
                }}
              />
            </View>

            <View style={{height: hp(15)}} />
          </Content>
        )}

        <Modal
          style={{justifyContent: 'flex-end'}}
          backdropOpacity={0.3}
          animationIn={'slideInUp'}
          animationOut={'slideOutDown'}
          isVisible={state.pickerVisible}
          onBackdropPress={() => togglePicker()}
          onBackButtonPress={() => togglePicker()}>
          <View style={{marginBottom: 10}}>
            <View
              style={{
                borderRadius: 5,
                flexDirection: 'row',
                paddingHorizontal: 30,
                paddingVertical: 40,
                backgroundColor: 'white',
              }}>
              <View style={{marginRight: 10, flex: 1}}>
                <Button transparent onPress={() => pickSingleWithCamera(false)}>
                  <View style={{alignItems: 'center'}}>
                    <Image
                      source={require('../../res/images/camera.png')}
                      style={{
                        height: 70,
                        width: 90,
                        resizeMode: 'contain',
                      }}
                    />
                    <View style={{width: '100%'}}>
                      <Text
                        style={{
                          fontSize: 12,
                          color: colors.greenpln,
                        }}>
                        Take a Photo
                      </Text>
                    </View>
                  </View>
                </Button>
              </View>
              <View style={{marginLeft: 10, flex: 1}}>
                <Button transparent onPress={() => pickSingle(false)}>
                  <View style={{alignItems: 'center'}}>
                    <Image
                      source={require('../../res/images/galery.png')}
                      style={{
                        height: 70,
                        width: 90,
                        resizeMode: 'contain',
                      }}
                    />
                    <View style={{width: '100%'}}>
                      <Text
                        style={{
                          fontSize: 12,
                          color: colors.greenpln,
                        }}>
                        Select from Galery
                      </Text>
                    </View>
                  </View>
                </Button>
              </View>
            </View>
          </View>

          <View style={{borderRadius: 5, marginBottom: 10}}>
            <Button
              onPress={() => togglePicker()}
              style={{backgroundColor: 'white'}}>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    color: colors.greenpln,
                    textAlign: 'center',
                  }}>
                  Cancel
                </Text>
              </View>
            </Button>
          </View>
        </Modal>
      </ImageBackground>
    </Container>
  );
};

export default AddUser;
