/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  ScrollView,
  Alert,
} from 'react-native';
import {Container, Content, Button} from 'native-base';
import colors from '../../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DefaultHeader from '../../component/DefaultHeader';
import {useNavigation, useRoute} from '@react-navigation/native';
import ReTextInput from '../../component/ReTextInput';
import RePicker from '../../component/RePicker';
import ReTextArea from '../../component/ReTextArea';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-crop-picker';
import Api from '../../services/Api';
import ReButtonSave from '../../component/ReButtonSave';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import {useGlobalContext} from '../../context/GlobalContext';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import storage from '@react-native-firebase/storage';
import uuid from 'react-native-uuid';

const listKelas = [
  {
    value: 'Kelas VI',
    label: 'Kelas VI',
  },
  {
    value: 'Kelas VII',
    label: 'Kelas VII',
  },
  {
    value: 'Kelas VIII',
    label: 'Kelas VIII',
  },
];

const AddTugas = () => {
  const {getGlobalState, setLoading} = useGlobalContext();

  const initialState = {
    pickerVisible: false,
    imageAviable: false,
    disabledSave: true,

    listPelajaran: [],
  };

  const initialFormData = {
    id: '',
    penulis: '',
    judul: '',
    pelajaran: '',
    kelas: '',
    isi: '',
    lampiran: [],
    dokumen: {},
    // penulis: 'Bambang',
    // judul: 'Matematika aljabar linier',
    // pelajaran: 'Matematika',
    // kelas: 'Kelas VIII',
    // isi: 'isi materi',
    imageObject: [],
    docObject: {},
  };
  const navigation = useNavigation();
  const route = useRoute();
  const [state, setState] = useState(initialState);
  const [loadingScreen, setLoadingScreen] = useState(false);
  const [formData, setFormData] = useState(initialFormData);
  const {inputTugas, updateTugas, pelajaran, tugas} = Api();

  useEffect(() => {
    checkParams();
    LoadPelajaran();
  }, []);

  const checkParams = async () => {
    const data = (await route.params) ? route.params.data : null;
    if (data !== null) {
      await setLoadingScreen(true);
      await updateFormData({
        id: data.id,
        penulis: data.penulis,
        judul: data.judul,
        pelajaran: data.pelajaran,
        kelas: data.kelas,
        isi: data.isi,
        lampiran: data.lampiran,
        dokumen: data.dokumen,
      });
      await updateState({imageAviable: true});

      await setLoadingScreen(false);
    } else {
      await setLoadingScreen(true);
      await updateFormData({
        penulis: getGlobalState('userData').nama,
      });
      await setLoadingScreen(false);
    }
  };

  const LoadPelajaran = async () => {
    await pelajaran.onSnapshot((snapshot) => {
      let plj = [];

      snapshot.forEach((doc) => {
        plj.push({
          label: doc.data().value,
          value: doc.data().value,
        });
      });
      updateState({listPelajaran: plj});
    });
  };

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const updateFormData = (newData) => {
    setFormData((prev) => ({...prev, ...newData}));
  };

  const pickSingle = (cropping, mediaType = 'photo') => {
    ImagePicker.openPicker({
      cropping: cropping,
      includeExif: true,
      compressImageQuality: 0.2,
      mediaType,
    })
      .then(async (image) => {
        let newImage = formData.imageObject;
        await newImage.push({
          uri: image.path,
          mime: image.mime,
        });

        await updateFormData({
          imageObject: newImage.reverse(),
        });
        updateState({
          imageAviable: true,
          pickerVisible: false,
        });
      })
      .catch((e) => {
        console.log(e);
        // Alert.alert(e.message ? e.message : e);
      });
  };

  const pickSingleWithCamera = (cropping, mediaType = 'photo') => {
    ImagePicker.openCamera({
      cropping: cropping,
      compressImageQuality: 0.2,
      includeExif: true,
      mediaType,
    })
      .then(async (image) => {
        let newImage = formData.imageObject;
        await newImage.push({
          uri: image.path,
          mime: image.mime,
        });

        await updateFormData({
          imageObject: newImage.reverse(),
        });
        updateState({
          imageAviable: true,
          pickerVisible: false,
        });
      })
      .catch((e) => alert(e));
    togglePicker();
  };

  const togglePicker = () => {
    updateState({pickerVisible: !state.pickerVisible});
  };

  const checkFilledForm = () => {
    if (
      formData.judul.length > 0 &&
      formData.penulis.length > 0 &&
      formData.isi.length > 0 &&
      formData.kelas.length > 0 &&
      formData.pelajaran.length > 0
    ) {
      return false;
    }

    return true;
  };

  const handleCreateData = async (doc) => {
    inputTugas(formData, doc).then((solv) => {
      console.log('berhasil input', solv);
    });
  };

  const handleUpdateData = async (doc) => {
    const solved = await updateTugas(formData, doc);
    console.log('berhasil update', solved);
  };

  const uploadDoc = async (doc) => {
    const uri = 'file://' + doc.path;
    const ext = uri.split('.').pop();
    const filename = `${uuid.v1()}.${ext}`;
    const imageRef = storage().ref(`tugas/${filename}`);
    await imageRef.putFile(uri);

    return await {name: filename, uri: await imageRef.getDownloadURL()};
  };

  const handleOnSave = async () => {
    await setLoading(true);
    if (Object.values(formData.docObject).length > 0) {
      await uploadDoc(formData.docObject).then((doc) => {
        if (formData.id.length > 0) {
          handleUpdateData(doc);
        } else {
          handleCreateData(doc);
        }
      });
    } else {
      if (formData.id.length > 0) {
        handleUpdateData();
      } else {
        handleCreateData();
      }
    }
    navigation.goBack();
  };

  const handleDeleteLocalImage = (index) => {
    const allImage = formData.imageObject;
    allImage.splice(index, 1);
    updateFormData({imageObject: allImage});
  };

  const handleDeleteServerImage = async (data, index) => {
    const allImage = formData.lampiran;
    await allImage.splice(index, 1);

    await tugas.doc(formData.id).update({
      lampiran: allImage,
    });
    updateFormData({lampiran: allImage});
  };

  const pickDocument = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });

      await updateFormData({dokumen: {}});

      await RNFetchBlob.fs.stat(res.uri).then((files) => {
        updateFormData({docObject: files});
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  return (
    <Container style={{flex: 1}}>
      <ImageBackground
        source={require('../../res/images/bg2.png')}
        style={{
          flex: 1,
          resizeMode: 'cover',
          // justifyContent: 'center',
        }}>
        <DefaultHeader
          title={route.params ? 'Edit Tugas' : 'Tambah Tugas'}
          backButton
          // rightButton="plus"
          // onPressButtonRight={() => navigation.navigate('AddMateri')}
          // searchable
        />

        {/* Content */}
        {loadingScreen ? (
          <View style={{paddingVertical: hp('2%')}}>
            <ActivityIndicator size={'large'} />
          </View>
        ) : (
          <Content style={{paddingHorizontal: wp(4)}}>
            <View style={{marginTop: hp(3), marginBottom: hp(1)}}>
              <Text
                style={{
                  fontFamily: 'Poppins-Regular',
                  fontSize: wp(4),
                  color: 'black',
                }}>
                *Isi Form Tugas
              </Text>
            </View>
            <View style={{marginVertical: hp(1.2)}}>
              <ReTextInput
                label="Pemberi Tugas"
                value={formData.penulis}
                onChangeText={(text) => {
                  updateFormData({penulis: text});
                }}
              />
            </View>

            <View style={{marginVertical: hp(1.2)}}>
              <RePicker
                label="Kelas"
                data={listKelas}
                value={formData.kelas}
                onValueChange={(value) => {
                  updateFormData({kelas: value});
                }}
              />
            </View>
            <View style={{marginVertical: hp(1.2)}}>
              <RePicker
                label="Mata Pelajaran"
                data={state.listPelajaran}
                value={formData.pelajaran}
                onValueChange={(value) => {
                  updateFormData({pelajaran: value});
                }}
              />
            </View>
            <View style={{marginVertical: hp(2)}}>
              <ReTextInput
                label="Judul"
                value={formData.judul}
                onChangeText={(text) => {
                  updateFormData({judul: text});
                }}
              />
            </View>
            <View style={{marginVertical: hp(1.2)}}>
              <ReTextArea
                label="Detail Tugas"
                value={formData.isi}
                onChangeText={(text) => {
                  updateFormData({isi: text});
                }}
              />
            </View>
            <View style={{marginVertical: hp(1.2)}}>
              <View
                style={{
                  borderWidth: 2,
                  paddingVertical: wp(3.5),
                  paddingHorizontal: wp(2),
                  borderColor: colors.blueDefault,
                  backgroundColor: 'rgba(255,255,255, 0.4)',
                  borderRadius: wp(1),
                  flexDirection: 'row',
                }}>
                <View style={{flex: 0.6, justifyContent: 'center'}}>
                  <FontAweSome
                    name="file-text-o"
                    size={wp(6)}
                    color={colors.blueDefault}
                  />
                </View>
                <View style={{flex: 6, justifyContent: 'center'}}>
                  {Object.values(formData.dokumen).length > 0 ? (
                    <Text
                      ellipsizeMode="tail"
                      numberOfLines={1}
                      style={{fontSize: wp(4)}}>
                      {Object.values(formData.dokumen).length > 0
                        ? formData.dokumen.name
                        : 'Select Dokumen'}
                    </Text>
                  ) : (
                    <Text
                      ellipsizeMode="tail"
                      numberOfLines={1}
                      style={{fontSize: wp(4)}}>
                      {Object.values(formData.docObject).length > 0
                        ? formData.docObject.filename
                        : 'Select Dokumen'}
                    </Text>
                  )}
                </View>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginHorizontal: wp(1),
                  }}>
                  <TouchableOpacity
                    onPress={() => pickDocument()}
                    style={{
                      backgroundColor: colors.gray11,
                      paddingHorizontal: wp(3),
                      paddingVertical: wp(2),
                      marginVertical: wp(-1.5),
                      borderRadius: wp(1),
                    }}>
                    <Text>Select</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View
              style={{
                flex: 1,
                borderColor: colors.blueDefault,
                borderWidth: 1,
                borderRadius: wp(1),
                marginVertical: hp(1.2),
              }}>
              <View
                style={{
                  backgroundColor: colors.blueDefault,
                  flexDirection: 'row',
                }}>
                <View style={{flex: 1, marginLeft: 10, padding: 5}}>
                  <Text style={{fontWeight: 'bold', color: 'white'}}>
                    Tambah Lampiran
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    alignItems: 'flex-end',
                    marginRight: 10,
                    padding: 5,
                  }}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      color: colors.danger,
                      display:
                        formData.imageObject.length >= 5 ? 'flex' : 'none',
                    }}>
                    Maximal Gambar : 5
                  </Text>
                </View>
              </View>
              <ScrollView horizontal style={{flexDirection: 'row'}}>
                <View
                  style={{
                    height: '100%',
                    paddingVertical: hp(2),
                    marginHorizontal: wp(2),
                    display: formData.imageObject.length >= 5 ? 'none' : 'flex',
                  }}>
                  <TouchableOpacity
                    style={{
                      backgroundColor: colors.soft1,
                      height: wp(23),
                      width: wp(17),
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRadius: wp(1),
                    }}
                    onPress={() => togglePicker()}>
                    <FontAweSome name="plus" size={wp(10)} color="white" />
                  </TouchableOpacity>
                </View>
                {formData.imageObject.map((data, i) => (
                  <View
                    key={i}
                    style={{
                      height: '100%',
                      marginHorizontal: wp(1.5),
                      paddingVertical: hp(2),
                    }}>
                    <TouchableOpacity>
                      <Image
                        source={{
                          uri: data.uri,
                        }}
                        style={{
                          borderRadius: wp(1),
                          height: wp(23),
                          width: wp(20),
                          resizeMode: 'cover',
                        }}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        handleDeleteLocalImage(i);
                      }}
                      style={{
                        position: 'absolute',
                        top: 8,
                        right: -4,
                        backgroundColor: colors.danger,
                        borderRadius: wp(10),
                        width: wp(6.5),
                        height: wp(6.5),
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <FontAweSome name="close" size={wp(3.5)} color="white" />
                    </TouchableOpacity>
                  </View>
                ))}
                {formData.lampiran.map((data, i) => (
                  <View
                    key={i}
                    style={{
                      height: '100%',
                      marginHorizontal: wp(1.5),
                      paddingVertical: hp(2),
                    }}>
                    <TouchableOpacity>
                      <Image
                        source={{
                          uri: data.uri,
                        }}
                        style={{
                          borderRadius: wp(1),
                          height: wp(23),
                          width: wp(20),
                          resizeMode: 'cover',
                        }}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        Alert.alert(
                          'Hapus Gambar',
                          'Lampiran ini akan di hapus permanen dari server dan tidak bisa di kembalikan',
                          [
                            {
                              text: 'Cancel',
                              style: 'cancel',
                            },
                            {
                              text: 'Hapus',
                              onPress: () => handleDeleteServerImage(data, i),
                            },
                          ],
                          {cancelable: false},
                        );
                      }}
                      style={{
                        position: 'absolute',
                        top: 8,
                        right: -4,
                        backgroundColor: colors.danger,
                        borderRadius: wp(10),
                        width: wp(6.5),
                        height: wp(6.5),
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <FontAweSome name="close" size={wp(3.5)} color="white" />
                    </TouchableOpacity>
                  </View>
                ))}

                <View style={{width: wp(3)}} />
              </ScrollView>
            </View>

            <ReButtonSave
              disabled={checkFilledForm()}
              onPress={() => {
                handleOnSave();
              }}
            />
            <View style={{height: hp(15)}} />
          </Content>
        )}

        <Modal
          style={{justifyContent: 'flex-end'}}
          backdropOpacity={0.3}
          animationIn={'slideInUp'}
          animationOut={'slideOutDown'}
          isVisible={state.pickerVisible}
          onBackdropPress={() => togglePicker()}
          onBackButtonPress={() => togglePicker()}>
          <View style={{marginBottom: 10}}>
            <View
              style={{
                borderRadius: 5,
                flexDirection: 'row',
                paddingHorizontal: 30,
                paddingVertical: 40,
                backgroundColor: 'white',
              }}>
              <View style={{marginRight: 10, flex: 1}}>
                <Button transparent onPress={() => pickSingleWithCamera(false)}>
                  <View style={{alignItems: 'center'}}>
                    <Image
                      source={require('../../res/images/camera.png')}
                      style={{
                        height: 70,
                        width: 90,
                        resizeMode: 'contain',
                      }}
                    />
                    <View style={{width: '100%'}}>
                      <Text
                        style={{
                          fontSize: 12,
                          color: colors.greenpln,
                        }}>
                        Take a Photo
                      </Text>
                    </View>
                  </View>
                </Button>
              </View>
              <View style={{marginLeft: 10, flex: 1}}>
                <Button transparent onPress={() => pickSingle(false)}>
                  <View style={{alignItems: 'center'}}>
                    <Image
                      source={require('../../res/images/galery.png')}
                      style={{
                        height: 70,
                        width: 90,
                        resizeMode: 'contain',
                      }}
                    />
                    <View style={{width: '100%'}}>
                      <Text
                        style={{
                          fontSize: 12,
                          color: colors.greenpln,
                        }}>
                        Select from Galery
                      </Text>
                    </View>
                  </View>
                </Button>
              </View>
            </View>
          </View>

          <View style={{borderRadius: 5, marginBottom: 10}}>
            <Button
              onPress={() => togglePicker()}
              style={{backgroundColor: 'white'}}>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    color: colors.greenpln,
                    textAlign: 'center',
                  }}>
                  Cancel
                </Text>
              </View>
            </Button>
          </View>
        </Modal>
      </ImageBackground>
    </Container>
  );
};

export default AddTugas;
