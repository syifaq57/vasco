/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground,
} from 'react-native';
import {Container, Header, Button, Content} from 'native-base';
import colors from '../../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import DefaultHeader from '../../component/DefaultHeader';
import TugasItem from './TugasItem';
import {useNavigation} from '@react-navigation/native';
import Api from '../../services/Api';
import {useGlobalContext} from '../../context/GlobalContext';

const TugasPage = () => {
  const navigation = useNavigation();
  const {tugas} = Api();
  const [search, setSearch] = useState('');
  const [listTugas, setTugas] = useState([]);
  const [loadingScreen, setLoadingScreen] = useState(false);
  const {getGlobalState, setGlobalState, setLoading} = useGlobalContext();
  const [state, setState] = useState({
    userData: '',
  });

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const loadTugas = async () => {
    await setLoadingScreen(true);
    await tugas.orderBy('createAt', 'desc').onSnapshot((snapshot) => {
      let Tugas = [];

      snapshot.forEach((doc) => {
        Tugas.push({
          id: doc.id,
          judul: doc.data().judul,
          kelas: doc.data().kelas,
          pelajaran: doc.data().pelajaran,
          penulis: doc.data().penulis,
          lampiran: doc.data().lampiran,
          isi: doc.data().isi,
          dokumen: doc.data().dokumen,
          // rate: doc.data().rate,
        });
      });
      // console.log('materi', MateriItem)
      if (getGlobalState('userData').kelas.length > 0) {
        setTugas(
          Tugas.filter((y) => y.kelas === getGlobalState('userData').kelas),
        );
      } else {
        setTugas(Tugas);
      }
    });
    await setLoadingScreen(false);
  };

  useEffect(() => {
    const subscriber = loadTugas();
    return () => subscriber;
  }, []);

  return (
    <Container style={{flex: 1}}>
      <ImageBackground
        source={require('../../res/images/bg2.png')}
        style={{
          flex: 1,
          resizeMode: 'cover',
          // justifyContent: 'center',
        }}>
        <DefaultHeader
          title="Tugas"
          backButton={getGlobalState('userData').role === 'murid' ? true : true}
          rightButton={
            getGlobalState('userData').role === 'murid' ? null : 'plus'
          }
          onPressButtonRight={() => navigation.navigate('AddTugas')}
          searchable
          onSearchChange={async (val) => {
            await setSearch(val);
          }}
        />

        {/* Content */}
        <Content style={{flex: 1, paddingHorizontal: wp(4)}}>
          <View style={{marginVertical: hp(1)}}>
            {loadingScreen ? (
              <View style={{paddingVertical: hp('2%')}}>
                <ActivityIndicator size={'large'} />
              </View>
            ) : (
              <TugasItem
                role={getGlobalState('userData').role}
                data={listTugas.filter((y) =>
                  y.judul.toLowerCase().includes(search),
                )}
              />
            )}
          </View>
          <View style={{height: hp(15)}} />
        </Content>
      </ImageBackground>
    </Container>
  );
};

export default TugasPage;
