/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  StatusBar,
  SafeAreaView,
  Alert,
  BackHandler,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import {Container, Header} from 'native-base';
import colors from '../../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Title from '../../component/Title';
import ItemGuru from './itemGuru';
import DefaultHeader from '../../component/DefaultHeader';
import {ScrollView} from 'react-native-gesture-handler';
import {useGlobalContext} from '../../context/GlobalContext';
import {useFocusEffect} from '@react-navigation/native';

const GuruPage = () => {
  const [state, setState] = useState({
    isLoading: false,
    userData: {},
  });
  const {setGlobalState} = useGlobalContext();

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const checkUserData = async () => {
    await updateState({isLoading: true});
    await AsyncStorage.getItem('userData').then((value) => {
      const data = JSON.parse(value);
      if (data !== null) {
        updateState({userData: data});
        setGlobalState({userData: data});
      }
    });
    await updateState({isLoading: false});
  };

  useEffect(() => {
    checkUserData();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        Alert.alert('Exit App!', 'Apakah Kamu yakin ingin keluar Aplikasi ?', [
          {
            text: 'Cancel',
            onPress: () => null,
            style: 'cancel',
          },
          {text: 'YES', onPress: () => BackHandler.exitApp()},
        ]);
        return true;
      };

      // Add Event Listener for hardwareBackPress
      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () => {
        // Once the Screen gets blur Remove Event Listener
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
      };
    }, []),
  );

  return (
    <Container style={{flex: 1}}>
      <ImageBackground
        source={require('../../res/images/bg2.png')}
        style={{
          flex: 1,
          resizeMode: 'cover',
          // justifyContent: 'center',
          backgroundColor: colors.grayBG,
        }}>
        <DefaultHeader title="Dashboard" dashboardRight />
        {/* Content */}
        <View>
          {state.isLoading ? (
            <View style={{paddingVertical: hp('2%')}}>
              <ActivityIndicator size={'large'} />
            </View>
          ) : (
            <ScrollView style={{paddingHorizontal: wp(4), paddingTop: hp(1)}}>
              <View style={{marginVertical: hp(1)}}>
                <Title backgroundColor={colors.soft1} />
              </View>
              <View style={{marginVertical: hp(1)}}>
                <ItemGuru
                  adminVisible={state.userData.role === 'Admin' ? true : false}
                />
              </View>
              <View style={{height: 120}} />
            </ScrollView>
          )}
        </View>
      </ImageBackground>
    </Container>
  );
};

export default GuruPage;
