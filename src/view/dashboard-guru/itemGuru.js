/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Card} from 'native-base';
import {View, TouchableOpacity, Text, Image, FlatList} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors';
import {useNavigation} from '@react-navigation/native';

const items = [
  {
    value: 'materi',
    label: 'Materi',
    img: require('../../res/images/materi.png'),
  },
  {
    value: 'tugas',
    label: 'Tugas',
    img: require('../../res/images/tugas.png'),
  },
  {
    value: 'murid',
    label: 'Murid',
    img: require('../../res/images/murid.png'),
  },
  {
    value: 'pelajaran',
    label: 'Mata Pelajaran',
    img: require('../../res/images/pelajaran.png'),
  },
  {
    value: 'admin',
    label: 'User',
    img: require('../../res/images/admin.png'),
  },
];

const itemGuru = (props) => {
  const navigation = useNavigation();

  const handlePressItem = (item) => {
    switch (item) {
      case 'materi':
        navigation.navigate('MateriPage');
        break;
      case 'tugas':
        navigation.navigate('TugasPage');
        break;
      case 'murid':
        navigation.navigate('ListMuridPage');
        break;
      case 'pelajaran':
        navigation.navigate('PelajaranPage');
        break;
      case 'admin':
        navigation.navigate('AdminPage');
        break;
      default:
        break;
    }
  };
  return (
    <View style={{flex: 1}}>
      <FlatList
        numColumns={2}
        data={items}
        renderItem={({item, index}) => (
          <TouchableOpacity
            style={
              props.adminVisible
                ? {flex: 1}
                : {flex: 1, display: item.value === 'admin' ? 'none' : null}
            }
            key={index}
            onPress={() => {
              handlePressItem(item.value);
            }}>
            <Card
              style={{
                height: hp(25),
                marginRight: wp(1),
                marginLeft: wp(1),
                marginBottom: hp(1),
                borderRadius: wp(2),
                backgroundColor: colors.blueDefault,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View>
                <Image
                  style={{width: wp(20), height: hp(20), resizeMode: 'contain'}}
                  source={item.img}
                />
              </View>
              <View style={{marginBottom: hp(2)}}>
                <Text
                  style={{
                    fontSize: wp(5),
                    color: colors.white,
                    fontFamily: 'JosefinSans-Bold',
                  }}>
                  {item.label}
                </Text>
              </View>
            </Card>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default itemGuru;
