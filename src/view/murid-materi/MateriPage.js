/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground,
} from 'react-native';
import {Container, Header, Button, Content} from 'native-base';
import colors from '../../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import DefaultHeader from '../../component/DefaultHeader';
import MateriItem from './MateriItem';
import { useNavigation, useRoute} from '@react-navigation/native';
import Api from '../../services/Api';
import {useGlobalContext} from '../../context/GlobalContext';
import RePicker from '../../component/RePicker';

const listKelas = [
  {
    value: 'Kelas VI',
    label: 'Kelas VI',
  },
  {
    value: 'Kelas VII',
    label: 'Kelas VII',
  },
  {
    value: 'Kelas VIII',
    label: 'Kelas VIII',
  },
];

const initialFormData = {
  id: '',
  penulis: '',
  judul: '',
  pelajaran: '',
  kelas: 'VII',
  isiMateri: '',
  lampiran: '',
  // penulis: 'Bambang',
  // judul: 'Matematika aljabar linier',
  // pelajaran: 'Matematika',
  // kelas: 'Kelas VIII',
  // isiMateri: 'isi materi',
  imageObject: [],
};

const initialState = {
  listPelajaran: [],
};

const MateriPage = () => {
  const navigation = useNavigation();
  const {materi, pelajaran} = Api();
  const [search, setSearch] = useState('');
  const [listMateri, setMateri] = useState([]);
  const [formData, setFormData] = useState(initialFormData);
  const [state, setState] = useState(initialState);
  const [loadingScreen, setLoadingScreen] = useState(false);
  const {getGlobalState, setGlobalState, setLoading} = useGlobalContext();
  const route = useRoute();

  const LoadPelajaran = async () => {
    await pelajaran.onSnapshot((snapshot) => {
      let plj = [];

      snapshot.forEach((doc) => {
        plj.push({
          label: doc.data().value,
          value: doc.data().value,
        });
      });
      updateState({listPelajaran: plj});
    });
  };

  useEffect(() => {
    LoadPelajaran();
    updateFormData({kelas: getGlobalState('userData').kelas});
    checkParams();

  }, []);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const updateFormData = (newData) => {
    setFormData((prev) => ({...prev, ...newData}));
  };

  const checkParams = async () => {
    const data = (await route.params) ? route.params.data : null;
    if (data !== null) {
      await setLoadingScreen(true);
      await updateFormData({
        pelajaran: data.value,
      });

      await setLoadingScreen(false);
    }
  };

  const loadMateri = async () => {
    await setLoadingScreen(true);
    await materi.orderBy('createAt', 'desc').onSnapshot((snapshot) => {
      let Materi = [];

      snapshot.forEach((doc) => {
        Materi.push({
          id: doc.id,
          judul: doc.data().judul,
          kelas: doc.data().kelas,
          pelajaran: doc.data().pelajaran,
          penulis: doc.data().penulis,
          lampiran: doc.data().lampiran,
          isi: doc.data().isi,
          dokumen: doc.data().dokumen,
        });
      });
      // console.log('materi', MateriItem)
      setMateri(Materi);
    });
    await setLoadingScreen(false);
  };

  useEffect(() => {
    const subscriber = loadMateri();
    return () => subscriber;
  }, []);

  return (
    <Container style={{flex: 1}}>
      <ImageBackground
        source={require('../../res/images/bg2.png')}
        style={{
          flex: 1,
          resizeMode: 'cover',
          // justifyContent: 'center',
        }}>
        <DefaultHeader
          title="Materi"
          backButton
          // rightButton="plus"
          // onPressButtonRight={() => navigation.navigate('AddMateri')}
          searchable
          onSearchChange={async (val) => {
            await setSearch(val);
          }}
        />

        {/* Content */}
        {loadingScreen ? (
          <View style={{paddingVertical: hp('2%')}}>
            <ActivityIndicator size={'large'} />
          </View>
        ) : (
          <View
            style={{
              marginTop: hp(2),
              paddingHorizontal: wp(3),
            }}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1, marginRight: wp(1)}}>
                <RePicker
                  label="Kelas"
                  backgroundColor={colors.blueDefault}
                  textColor={colors.white}
                  data={listKelas}
                  value={formData.kelas}
                  onValueChange={(value) => {
                    updateFormData({kelas: value});
                  }}
                />
              </View>
              <View style={{flex: 1, marginLeft: wp(1)}}>
                <RePicker
                  label="Pelajaran"
                  backgroundColor={colors.blueDefault}
                  textColor="white"
                  data={state.listPelajaran}
                  value={formData.pelajaran}
                  onValueChange={(value) => {
                    updateFormData({pelajaran: value});
                  }}
                />
              </View>
              <View
                style={{
                  borderRadius: wp(1),
                  flex: 0.3,
                  backgroundColor: colors.soft1,
                  marginLeft: wp(1),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={async () => {
                    await setLoadingScreen(true);
                    updateFormData({pelajaran: ''});
                    await setLoadingScreen(false);
                  }}
                  style={{alignItems: 'center'}}>
                  <FontAweSome name="undo" color="white" size={wp(6)} />
                  <Text style={{fontSize: wp(3), color: 'white'}}>Reset</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        )}

        <Content style={{flex: 1, paddingHorizontal: wp(4)}}>
          <View style={{marginVertical: hp(1)}}>
            {loadingScreen ? (
              <View style={{paddingVertical: hp('2%')}}>
                <ActivityIndicator size={'large'} />
              </View>
            ) : (
              <MateriItem
                data={listMateri
                  .filter((y) =>
                    y.judul.toLowerCase().includes(search.toLowerCase()),
                  )
                  .filter((x) =>
                    x.pelajaran
                      .toLowerCase()
                      .includes(formData.pelajaran.toLowerCase()),
                  )
                  .filter((z) => z.kelas === formData.kelas)}
              />
            )}
          </View>
          <View style={{height: hp(15)}} />
        </Content>
      </ImageBackground>
    </Container>
  );
};

export default MateriPage;
