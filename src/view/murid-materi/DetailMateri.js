/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground,
  Linking,
} from 'react-native';
import {Container, Header} from 'native-base';
import colors from '../../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import DefaultHeader from '../../component/DefaultHeader';
import Collapsible from 'react-native-collapsible';
import {TabBar, TabView, SceneMap} from 'react-native-tab-view';
import {useNavigation, useRoute} from '@react-navigation/native';
import Modal from 'react-native-modal';
import ImageView from 'react-native-image-viewing';

const initialState = {
  bodyShow: true,
  documentShow: false,
  routes: [
    {key: 'Materi', title: 'Materi'},
    {key: 'Lampiran', title: 'Lampiran'},
    // {key: 'Gambar', title: 'Gambar'},
  ],
  index: 0,
};

const initialFormData = {
  id: '',
  penulis: '',
  judul: '',
  pelajaran: '',
  kelas: '',
  isiMateri: '',
  lampiran: [],
  dokumen: {},
};

const DetailMateri = () => {
  const [state, setState] = useState(initialState);
  const [loadingScreen, setLoadingScreen] = useState(false);
  const [openImage, setOpenImage] = useState({
    visible: false,
    index: 0,
  });
  const navigation = useNavigation();
  const [formData, setFormData] = useState(initialFormData);
  const route = useRoute();
  const updateState = (newData) => {
    setState((prev) => ({
      ...prev,
      ...newData,
    }));
  };

  const updateOpenImage = (newData) => {
    setOpenImage((prev) => ({
      ...prev,
      ...newData,
    }));
  };

  const updateFormData = (newData) => {
    setFormData((prev) => ({...prev, ...newData}));
  };

  useEffect(() => {
    checkParams();
  }, []);

  const checkParams = async () => {
    const data = route.params.data;
    await setLoadingScreen(true);
    await updateFormData({
      id: data.id,
      penulis: data.penulis,
      judul: data.judul,
      pelajaran: data.pelajaran,
      kelas: data.kelas,
      isiMateri: data.isi,
      lampiran: data.lampiran,
      dokumen: data.dokumen,
    });
    await setLoadingScreen(false);
    console.log('form data ', formData);
  };

  const openDokumen = (url) => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        Linking.openURL(url);
      } else {
        alert('Browser tidak ditemukan!');
        console.log("Don't know how to open URI: " + url);
      }
    });
  };

  const MateriTitle = () => {
    return (
      <View
        style={{
          padding: wp(3),
        }}>
        <Text
          style={{
            fontSize: wp(6),
            fontFamily: 'Poppins-Regular',
            color: colors.lightBlack,
          }}>
          {formData.judul}
        </Text>
        <View style={{flexDirection: 'row'}}>
          <Text
            style={{
              color: 'grey',
              fontStyle: 'italic',
              fontSize: wp(3),
            }}>
            Oleh :{' '}
          </Text>
          <Text
            style={{
              color: 'grey',
              fontStyle: 'italic',
              fontSize: wp(3),
            }}>
            {formData.penulis}
          </Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text
            style={{
              color: 'grey',
              fontStyle: 'italic',
              fontSize: wp(3),
            }}>
            Mata Pelajaran :{' '}
          </Text>
          <Text
            style={{
              color: 'grey',
              fontStyle: 'italic',
              fontSize: wp(3),
            }}>
            {formData.pelajaran}
          </Text>
        </View>
      </View>
    );
  };

  const Materi = () => {
    return (
      <ScrollView style={{marginBottom: wp(0)}}>
        {loadingScreen ? (
          <View style={{paddingVertical: hp('2%')}}>
            <ActivityIndicator size={'large'} />
          </View>
        ) : (
          <View>
            <MateriTitle />
            <View style={{marginHorizontal: wp(2.5), marginTop: hp(1)}}>
              <View>
                <Text>{formData.isiMateri}</Text>
              </View>
            </View>
          </View>
        )}
      </ScrollView>
    );
  };

  const Lampiran = () => {
    return (
      <ScrollView style={{marginBottom: wp(0)}}>
        {loadingScreen ? (
          <View style={{paddingVertical: hp('2%')}}>
            <ActivityIndicator size={'large'} />
          </View>
        ) : (
          <View>
            <MateriTitle />
            <View style={{marginHorizontal: wp(2.5), marginTop: hp(1)}}>
              <View>
                <Text style={{marginBottom: hp(2), fontWeight: 'bold'}}>
                  Image
                </Text>
                <ScrollView horizontal style={{flexDirection: 'row'}}>
                  {formData.lampiran.map((data, index) => (
                    <View key={index} style={{marginRight: wp(2)}}>
                      <TouchableOpacity
                        onPress={() =>
                          updateOpenImage({visible: true, index: index})
                        }>
                        <Image
                          source={{uri: data.uri}}
                          style={{
                            height: wp(30),
                            width: wp(30),
                            resizeMode: 'cover',
                          }}
                        />
                      </TouchableOpacity>
                    </View>
                  ))}
                </ScrollView>

                <ImageView
                  images={formData.lampiran}
                  imageIndex={openImage.index}
                  visible={openImage.visible}
                  onRequestClose={() => setOpenImage({visible: false})}
                />
              </View>
              <View style={{marginVertical: hp(4)}}>
                <Text style={{marginBottom: hp(2), fontWeight: 'bold'}}>
                  Dokumen
                </Text>
                {Object.values(formData.dokumen).length > 0 ? (
                  <View
                    style={{
                      height: wp(30),
                      width: wp(25),
                    }}>
                    <TouchableOpacity
                      onPress={() => openDokumen(formData.dokumen.uri)}>
                      <Image
                        source={require('../../res/images/document.png')}
                        style={{
                          height: wp(30),
                          width: wp(25),
                          resizeMode: 'contain',
                        }}
                      />
                      <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
                        Open
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : null}
              </View>
            </View>
          </View>
        )}
      </ScrollView>
    );
  };

  return (
    <Container style={{flex: 1}}>
      <ImageBackground
        source={require('../../res/images/bg2.png')}
        style={{
          flex: 1,
          resizeMode: 'cover',
          // justifyContent: 'center',
        }}>
        <DefaultHeader title="Detail Materi" backButton />

        {/* Content */}
        <View style={{paddingHorizontal: wp(0), flex: 1, height: '100%'}}>
          <TabView
            navigationState={state}
            renderScene={SceneMap({
              Materi: Materi,
              Lampiran: Lampiran,
            })}
            onIndexChange={(index) => updateState({index: index})}
            // initialLayout={{width: Dimensions.get('window').width}}
            renderTabBar={(props) => (
              <TabBar
                {...props}
                indicatorStyle={{backgroundColor: 'white'}}
                style={{width: '100%', backgroundColor: colors.blueDefault}}
                labelStyle={{fontWeight: 'bold', fontSize: 12}}
              />
            )}
          />
        </View>
      </ImageBackground>
    </Container>
  );
};

export default DetailMateri;
