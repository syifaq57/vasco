/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, Text, Image, FlatList} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors';
import {Card} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';
import Api from '../../services/Api';

const randomColor = [{}];

const listPelajaran = [
  {
    materi: 'matematika',
    title: 'Perbandingan dan Aritmetika Sosial',
    link: 'https://drive.google.com/file/d/0B08C4WiimKyfd3RrMjktOXdFOW8/view',
  },
  {
    materi: 'matematika',
    title: 'Operasi hitung bentuk aljabar',
    link: 'https://drive.google.com/file/d/0B08C4WiimKyfd3RrMjktOXdFOW8/view',
  },
  {
    materi: 'fisika',
    title: 'Wujud zat dan massa jenis',
    link: 'https://drive.google.com/file/d/0B08C4WiimKyfd3RrMjktOXdFOW8/view',
  },
  {
    materi: 'fisika',
    title: 'Usaha dan energi',
    link: 'https://drive.google.com/file/d/0B08C4WiimKyfd3RrMjktOXdFOW8/view',
  },
  {
    materi: 'binggris',
    title: 'Expressing Gratitude',
    link: 'https://drive.google.com/file/d/0B08C4WiimKyfd3RrMjktOXdFOW8/view',
  },
  {
    materi: 'binggris',
    title: 'Grammar',
    link: 'https://drive.google.com/file/d/0B08C4WiimKyfd3RrMjktOXdFOW8/view',
  },
];

const MateriItem = (props) => {
  // const others = {
  //   materi: 'lain',
  //   label: 'Lihat Lainnya',
  //   link: 'https://drive.google.com/file/d/0B08C4WiimKyfd3RrMjktOXdFOW8/view',
  // };
  const [dataMap, setDataMap] = useState(props.data);
  const navigation = useNavigation();
  const {deleteMateri} = Api();

  const setIconMateri = (materi) => {
    if (materi === 'Matematika') {
      return require('../../res/images/matematika.png');
    } else if (materi === 'Fisika') {
      return require('../../res/images/fisika.png');
    } else if (materi === 'B.inggris') {
      return require('../../res/images/inggris.png');
    } else if (materi === 'Sosiologi') {
      return require('../../res/images/sosiologi.png');
    } else if (materi === 'Biologi') {
      return require('../../res/images/biologi.png');
    } else if (materi === 'Sejarah') {
      return require('../../res/images/sejarah.png');
    } else if (materi === 'Kimia') {
      return require('../../res/images/kimia.png');
    } else if (materi === 'Geografi') {
      return require('../../res/images/geografi.png');
    } else if (materi === 'B.indo') {
      return require('../../res/images/indonesia.png');
    }
    return require('../../res/images/materi.png');
  };

  useEffect(() => {
    setDataMap(props.data);
  }, [props.data]);

  const navigateTo = (page, data) => {
    navigation.navigate(page, {
      data: data,
    });
  };

  const RenderItem = ({item, index}) => {
    const [iconList, setIconList] = useState(setIconMateri(item.pelajaran));
    const [state, setState] = useState({
      editable: false,
    });
    return (
      <View
        key={index}
        style={{
          flexDirection: 'row',
          height: hp(10),
          width: '100%',
          marginBottom: hp(2),
        }}>
        <TouchableOpacity
          onPress={() => navigateTo('MuridDetailMateri', item)}
          style={{
            flex: 4,
            backgroundColor: index % 2 === 0 ? colors.soft2 : colors.white,
            borderRadius: hp(1),
            padding: wp(2),
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Image
              style={{width: wp(10), height: hp(5), resizeMode: 'contain'}}
              source={iconList}
            />
          </View>
          <View style={{flex: 5, marginLeft: wp(1)}}>
            <Text
              ellipsizeMode="tail"
              numberOfLines={2}
              style={{fontSize: wp(3), fontFamily: 'Poppins-Regular'}}>
              {item.judul}
            </Text>
            <Text
              ellipsizeMode="tail"
              numberOfLines={2}
              style={{fontSize: wp(2), fontFamily: 'Poppins-Regular'}}>
              {item.pelajaran}
            </Text>
          </View>
          {state.editable ? (
            <View
              style={{
                flex: 1.2,
                justifyContent: 'center',
                flexDirection: 'row',
              }}>
              <TouchableOpacity
                onPress={() => {
                  navigateTo('AddMateri', item);
                }}
                style={{marginRight: wp(3)}}>
                <Icon name="pencil" size={wp(5.3)} color={colors.blueDefault} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => deleteMateri(item)}>
                <Icon name="trash" size={wp(5.3)} color={colors.blueDefault} />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={{flex: 0.5}}>
              <Icon
                name="chevron-right"
                size={wp(4)}
                color={colors.blueDefault}
              />
            </View>
          )}
        </TouchableOpacity>
      </View>
    );
  };

  return (
    // <Card style={{borderRadius: hp(2), backgroundColor: colors.soft1}}>
    <View style={{flex: 1, marginVertical: hp(2)}}>
      <FlatList
        // style={{}}
        // horizontal={true}
        data={dataMap}
        renderItem={({item, index}) => <RenderItem item={item} index={index} />}
      />
    </View>
    // </Card>
  );
};

export default MateriItem;
