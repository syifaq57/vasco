/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground,
} from 'react-native';
import {Container, Header, Button, Content} from 'native-base';
import colors from '../../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import firestore from '@react-native-firebase/firestore';
import ReTextInput from '../../component/ReTextInput';
import {ScrollView} from 'react-native-gesture-handler';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import DefaultHeader from '../../component/DefaultHeader';
import PelajaranItem from '../admin-pelajaran/PelajaranItem';
import {useNavigation} from '@react-navigation/native';
import Api from '../../services/Api';
import Modal from 'react-native-modal';
import ReButtonSave from '../../component/ReButtonSave';

const PelajaranPage = () => {
  const navigation = useNavigation();
  const {pelajaran} = Api();
  const [search, setSearch] = useState('');
  const [listPelajaran, setPelajaran] = useState([]);
  const [loadingScreen, setLoadingScreen] = useState(false);
  const [visibleAdd, setVisibleAdd] = useState(false);
  const [formData, setFormData] = useState({
    pelajaran: '',
  });

  const toggleShowAdd = () => {
    setVisibleAdd(!visibleAdd);
  };

  const loadPelajaran = async () => {
    await setLoadingScreen(true);
    await pelajaran.onSnapshot((snapshot) => {
      let Pelajaran = [];

      snapshot.forEach((doc) => {
        Pelajaran.push({
          id: doc.id,
          name: doc.data().value,
        });
      });
      setPelajaran(Pelajaran);
    });
    await setLoadingScreen(false);
  };

  useEffect(() => {
    const subscriber = loadPelajaran();
    return () => subscriber;
  }, []);

  const checkFilledForm = () => {
    if (formData.pelajaran.length > 0) {
      return false;
    }

    return true;
  };

  const handleOnSave = async () => {
    pelajaran.add({
      value: formData.pelajaran,
    });
    await setFormData({...formData, pelajaran: ''});
    setVisibleAdd(false);
  };

  const deletePelajaran = (data) => {
    pelajaran.doc(data.id).delete();
    // this.props.reload();
  };

  return (
    <Container style={{flex: 1}}>
      <ImageBackground
        source={require('../../res/images/bg2.png')}
        style={{
          flex: 1,
          resizeMode: 'cover',
          // justifyContent: 'center',
        }}>
        <DefaultHeader
          title="Daftar Pelajaran"
          backButton
          rightButton="plus"
          onPressButtonRight={() => toggleShowAdd()}
          searchable
          onSearchChange={async (val) => {
            await setSearch(val);
          }}
        />

        {/* Content */}
        <Content style={{flex: 1, paddingHorizontal: wp(4)}}>
          <View style={{marginVertical: hp(1)}}>
            {loadingScreen ? (
              <View style={{paddingVertical: hp('2%')}}>
                <ActivityIndicator size={'large'} />
              </View>
            ) : (
              <PelajaranItem
                onDelete={(item) => deletePelajaran(item)}
                data={listPelajaran.filter((y) =>
                  y.name.toLowerCase().includes(search.toLowerCase()),
                )}
              />
            )}
          </View>
          <View style={{height: hp(15)}} />
        </Content>
        <Modal
          animationIn={'slideInDown'}
          isVisible={visibleAdd}
          onBackdropPress={() => toggleShowAdd()}
          backdropOpacity={0.3}
          animationOut={'slideOutUp'}
          animationInTiming={1000}>
          <View
            style={{
              // position: 'absolute',
              maxHeight: hp(45),
              backgroundColor: colors.white,
              borderRadius: wp(1),
            }}>
            <View
              style={{
                backgroundColor: colors.blueDefault,
                padding: wp(4),
                borderTopRightRadius: wp(1),
                borderTopLeftRadius: wp(1),
              }}>
              <Text
                style={{fontWeight: 'bold', fontSize: wp(4), color: 'white'}}>
                Tambah Pelajaran
              </Text>
            </View>
            <View style={{paddingHorizontal: wp(2)}}>
              <View style={{marginBottom: hp(1.5), marginTop: hp(3)}}>
                <ReTextInput
                  label="Nama Pelajaran"
                  value={formData.pelajaran}
                  onChangeText={(text) => {
                    setFormData({...formData, pelajaran: text});
                  }}
                />
              </View>
              <View style={{marginBottom: hp(2)}}>
                <ReButtonSave
                  disabled={checkFilledForm()}
                  onPress={() => {
                    handleOnSave();
                  }}
                />
              </View>
            </View>
          </View>
        </Modal>
      </ImageBackground>
    </Container>
  );
};

export default PelajaranPage;
