/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, Text, Image, FlatList} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors';
import {Card} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';
import Api from '../../services/Api';

const PelajaranItem = (props) => {
  const [dataMap, setDataMap] = useState(props.data);
  const navigation = useNavigation();
  const {deleteMurid} = Api();

  const setIconMateri = (materi) => {
    if (materi === 'Matematika') {
      return require('../../res/images/matematika.png');
    } else if (materi === 'Fisika') {
      return require('../../res/images/fisika.png');
    } else if (materi === 'B.Inggris') {
      return require('../../res/images/inggris.png');
    } else if (materi === 'Sosiologi') {
      return require('../../res/images/sosiologi.png');
    } else if (materi === 'Biologi') {
      return require('../../res/images/biologi.png');
    } else if (materi === 'Sejarah') {
      return require('../../res/images/sejarah.png');
    } else if (materi === 'Kimia') {
      return require('../../res/images/kimia.png');
    } else if (materi === 'Geografi') {
      return require('../../res/images/geografi.png');
    } else if (materi === 'B.Indo') {
      return require('../../res/images/indonesia.png');
    }
    return require('../../res/images/materi.png');
  };

  // useEffect(() => {
  //   setDataMap(props.data);
  // }, [props.data]);

  const navigateTo = (page, data) => {
    navigation.navigate(page, {
      data: data,
    });
  };

  const RenderItem = ({item, index}) => {
    const [iconList, setIconList] = useState(setIconMateri(item.pelajaran));
    const [state, setState] = useState({
      editable: true,
    });
    return (
      <View
        key={index}
        style={{
          flexDirection: 'row',
          height: hp(10),
          width: '100%',
          marginBottom: hp(2),
        }}>
        <TouchableOpacity
          // onPress={() => navigateTo('AddUser', item)}
          style={{
            flex: 4,
            backgroundColor: index % 2 === 0 ? colors.soft2 : colors.white,
            borderRadius: hp(1),
            padding: wp(2),
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Image
              style={{
                width: wp(10),
                height: wp(10),
                borderRadius: wp(50),
                resizeMode: 'cover',
              }}
              source={setIconMateri(item.name)}
            />
          </View>
          <View style={{flex: 5, marginLeft: wp(1)}}>
            <Text
              ellipsizeMode="tail"
              numberOfLines={2}
              style={{fontSize: wp(3), fontFamily: 'Poppins-Regular'}}>
              {item.name}
            </Text>
            {/* <Text
              ellipsizeMode="tail"
              numberOfLines={2}
              style={{fontSize: wp(2), fontFamily: 'Poppins-Regular'}}>
              {item.role.toUpperCase()}
            </Text> */}
          </View>
          {state.editable ? (
            <View
              style={{
                flex: 1.2,
                justifyContent: 'center',
                flexDirection: 'row',
              }}>
              {/* <TouchableOpacity
                onPress={() => {
                  navigateTo('AddMurid', item);
                }}
                style={{marginRight: wp(3)}}>
                <Icon name="pencil" size={wp(5.3)} color={colors.blueDefault} />
              </TouchableOpacity> */}
              <TouchableOpacity onPress={() => props.onDelete(item)}>
                <Icon name="trash" size={wp(5.3)} color={colors.blueDefault} />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={{flex: 1}}>
              <Icon
                name="chevron-right"
                size={wp(5)}
                color={colors.blueDefault}
              />
            </View>
          )}
        </TouchableOpacity>
      </View>
    );
  };

  return (
    // <Card style={{borderRadius: hp(2), backgroundColor: colors.soft1}}>
    <View style={{flex: 1, marginVertical: hp(2)}}>
      <FlatList
        // style={{}}
        // horizontal={true}
        data={props.data}
        renderItem={({item, index}) => <RenderItem item={item} index={index} />}
      />
    </View>
    // </Card>
  );
};

export default PelajaranItem;
