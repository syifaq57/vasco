/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground,
} from 'react-native';
import {Container, Header, Button, Content} from 'native-base';
import colors from '../../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import DefaultHeader from '../../component/DefaultHeader';
import MateriItem from './MateriItem';
import {useNavigation} from '@react-navigation/native';
import Api from '../../services/Api';

const MateriPage = () => {
  const navigation = useNavigation();
  const {materi} = Api();
  const [search, setSearch] = useState('');
  const [listMateri, setMateri] = useState([]);
  const [loadingScreen, setLoadingScreen] = useState(false);

  const loadMateri = async () => {
    await setLoadingScreen(true);
    await materi.orderBy('createAt', 'desc').onSnapshot((snapshot) => {
      let Materi = [];

      snapshot.forEach((doc) => {
        Materi.push({
          id: doc.id,
          judul: doc.data().judul,
          kelas: doc.data().kelas,
          pelajaran: doc.data().pelajaran,
          penulis: doc.data().penulis,
          lampiran: doc.data().lampiran,
          isi: doc.data().isi,
          dokumen: doc.data().dokumen,
        });
      });
      // console.log('materi', MateriItem)
      setMateri(Materi);
    });
    await setLoadingScreen(false);
  };

  useEffect(() => {
    const subscriber = loadMateri();
    return () => subscriber;
  }, []);

  return (
    <Container style={{flex: 1}}>
      <ImageBackground
        source={require('../../res/images/bg2.png')}
        style={{
          flex: 1,
          resizeMode: 'cover',
          // justifyContent: 'center',
        }}>
        <DefaultHeader
          title="Materi"
          backButton
          rightButton="plus"
          onPressButtonRight={() => navigation.navigate('AddMateri')}
          searchable
          onSearchChange={async (val) => {
            await setSearch(val);
          }}
        />

        {/* Content */}
        <Content style={{flex: 1, paddingHorizontal: wp(4)}}>
          <View style={{marginVertical: hp(1)}}>
            {loadingScreen ? (
              <View style={{paddingVertical: hp('2%')}}>
                <ActivityIndicator size={'large'} />
              </View>
            ) : (
              <MateriItem
                data={listMateri.filter((y) =>
                  y.judul.toLowerCase().includes(search),
                )}
              />
            )}
          </View>
          <View style={{height: hp(15)}} />
        </Content>
      </ImageBackground>
    </Container>
  );
};

export default MateriPage;
