/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, Text, Image, FlatList} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors';
import {Card} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';
import Api from '../../services/Api';

const MateriItem = () => {
  const {materi} = Api();
  const navigation = useNavigation();
  const [state, setState] = useState({
    materi: [],
    loadingMateri: false,
  });

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const loadMateri = async () => {
    await updateState({loadMateri: true});
    await materi.orderBy('createAt', 'desc').onSnapshot((snapshot) => {
      let Materi = [];

      snapshot.forEach((doc) => {
        Materi.push({
          id: doc.id,
          judul: doc.data().judul,
          kelas: doc.data().kelas,
          pelajaran: doc.data().pelajaran,
          penulis: doc.data().penulis,
          lampiran: doc.data().lampiran,
          isi: doc.data().isi,
          dokumen: doc.data().dokumen,
        });
      });
      // console.log('materi', MateriItem)
      Materi.splice(5);
      updateState({materi: Materi});
    });
    await updateState({loadMateri: false});
  };

  useEffect(() => {
    const subscriber = loadMateri();
    return () => subscriber;
  }, []);

  const setIconMateri = (materi) => {
    if (materi === 'Matematika') {
      return require('../../res/images/matematika.png');
    } else if (materi === 'Fisika') {
      return require('../../res/images/fisika.png');
    } else if (materi === 'B.inggris') {
      return require('../../res/images/inggris.png');
    } else if (materi === 'Sosiologi') {
      return require('../../res/images/sosiologi.png');
    } else if (materi === 'Biologi') {
      return require('../../res/images/biologi.png');
    } else if (materi === 'Sejarah') {
      return require('../../res/images/sejarah.png');
    } else if (materi === 'Kimia') {
      return require('../../res/images/kimia.png');
    } else if (materi === 'Geografi') {
      return require('../../res/images/geografi.png');
    } else if (materi === 'B.indo') {
      return require('../../res/images/indonesia.png');
    }
    return require('../../res/images/materi.png');
  };
  const navigateTo = (page, data) => {
    navigation.navigate(page, {
      data: data,
    });
  };

  const RenderItem = ({item, index}) => {
    const [iconList] = useState(setIconMateri(item.pelajaran));
    return (
      <View
        key={index}
        style={{
          flexDirection: 'row',
          height: hp(10),
          width: '100%',
          marginBottom: hp(2),
        }}>
        <TouchableOpacity
          onPress={() => navigateTo('MuridDetailMateri', item)}
          style={{
            flex: 4,
            backgroundColor: index % 2 === 0 ? colors.soft2 : colors.white,
            borderRadius: hp(1),
            padding: wp(2),
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Image
              style={{width: wp(10), height: hp(5), resizeMode: 'contain'}}
              source={iconList}
            />
          </View>
          <View style={{flex: 5, marginLeft: wp(1)}}>
            <Text
              ellipsizeMode="tail"
              numberOfLines={2}
              style={{fontSize: wp(3), fontFamily: 'Poppins-Regular'}}>
              {item.judul}
            </Text>
            <Text
              ellipsizeMode="tail"
              numberOfLines={2}
              style={{fontSize: wp(2), fontFamily: 'Poppins-Regular'}}>
              {item.pelajaran}
            </Text>
          </View>
          <View style={{flex: 0.5}}>
            <Icon
              name="chevron-right"
              size={wp(4)}
              color={colors.blueDefault}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <Card style={{borderRadius: hp(2), backgroundColor: colors.soft1}}>
      <View style={{marginVertical: hp(2)}}>
        <View
          style={{
            paddingLeft: wp(4),
            marginBottom: hp(1),
          }}>
          <Text
            style={{
              fontSize: wp(4),
              color: colors.white,
              fontFamily: 'Poppins-SemiBold',
            }}>
            * Materi Baru
          </Text>
        </View>
        <View style={{paddingHorizontal: hp(2)}}>
          <FlatList
            style={{flex: 1, height: '100%', paddingBottom: 20}}
            // horizontal={true}
            data={state.materi}
            renderItem={({item, index}) => (
              <RenderItem
                item={item}
                index={index}
                navigate={navigation.navigate}
              />
            )}
          />
          <TouchableOpacity
            onPress={() => navigation.navigate('MuridMateriPage')}
            style={{
              marginTop: hp(-3),
              backgroundColor: colors.gray02,
              borderRadius: hp(1),
              padding: wp(2),
              minHeight: hp(8.5),
            }}>
            <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: wp(5),
                  color: colors.white,
                  fontFamily: 'JosefinSans-Bold',
                }}>
                Lihat Lainnya
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Card>
  );
};

export default MateriItem;
