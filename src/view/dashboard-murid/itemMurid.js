/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Card} from 'native-base';
import {View, TouchableOpacity, Text, Image, FlatList} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors';
import {useNavigation} from '@react-navigation/native';

const items = [
  {
    value: 'materi',
    label: 'Materi',
    detail: 'Cari Materi & Baca Materi',
    img: require('../../res/images/materi.png'),
  },
  {
    value: 'tugas',
    label: 'Tugas',
    detail: 'Daftar Tugas & Kerjakan Tugas',
    img: require('../../res/images/tugas.png'),
  },
];

const itemMurid = (props) => {
  const navigation = useNavigation();

  const handlePressItem = (item) => {
    switch (item) {
      case 'materi':
        navigation.navigate('MuridMateriPage');
        break;
      case 'tugas':
        navigation.navigate('TugasPage');
        break;
      default:
        break;
    }
  };
  return (
    <View style={{flex: 1}}>
      <FlatList
        // numColumns={1}
        data={items}
        renderItem={({item, index}) => (
          <TouchableOpacity
            style={
              props.adminVisible
                ? {flex: 1}
                : {flex: 1, display: item.value === 'admin' ? 'none' : null}
            }
            key={index}
            onPress={() => {
              handlePressItem(item.value);
            }}>
            <Card
              style={{
                height: hp(20),
                marginRight: wp(1),
                marginLeft: wp(1),
                marginBottom: hp(1),
                borderRadius: wp(2),
                backgroundColor: colors.blueDefault,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
                paddingHorizontal: wp(3),
              }}>
              <View style={{flex: 1, marginRight: wp(2)}}>
                <Image
                  style={{width: wp(18), height: hp(18), resizeMode: 'contain'}}
                  source={item.img}
                />
              </View>
              <View style={{flex: 3}}>
                <Text
                  style={{
                    fontSize: wp(8),
                    color: colors.white,
                    fontFamily: 'JosefinSans-Bold',
                  }}>
                  {item.label}
                </Text>
                <Text
                  style={{
                    marginLeft: wp(1),
                    fontSize: wp(3.5),
                    color: colors.white,
                    fontFamily: 'JosefinSans-Regular',
                  }}>
                  {item.detail}
                </Text>
              </View>
            </Card>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default itemMurid;
