/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, Text, Image, FlatList} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors';
import {Card} from 'native-base';
import {useNavigation} from '@react-navigation/native';
import Api from '../../services/Api';

const listPelajaran = [
  {
    value: 'Matematika',
    label: 'Matematika',
    img: require('../../res/images/matematika.png'),
  },
  {
    value: 'Fisika',
    label: 'Fisika',
    img: require('../../res/images/fisika.png'),
  },
  {
    value: 'Biologi',
    label: 'Biologi',
    img: require('../../res/images/biologi.png'),
  },
  {
    value: 'B.Inggris',
    label: 'B.Inggris',
    img: require('../../res/images/inggris.png'),
  },
  {
    value: 'Kimia',
    label: 'Kimia',
    img: require('../../res/images/kimia.png'),
  },
  {
    value: 'B.Indo',
    label: 'B.Indonesia',
    img: require('../../res/images/indonesia.png'),
  },
  {
    value: 'Sejarah',
    label: 'Sejarah',
    img: require('../../res/images/sejarah.png'),
  },
  {
    value: 'Geografi',
    label: 'Geografi',
    img: require('../../res/images/geografi.png'),
  },
  {
    value: 'Sosiologi',
    label: 'Sosiologi',
    img: require('../../res/images/sosiologi.png'),
  },
];

const PelajaranItem = () => {
  const others = {
    value: 'lain',
    label: 'Lainnya',
    img: require('../../res/images/lain.png'),
  };
  const navigation = useNavigation();

  const [dataMap, setDataMap] = useState(listPelajaran);

  useEffect(() => {
    const newData = listPelajaran;
    newData.splice(5);
    newData.push(others);
    setDataMap(newData);
  }, []);

  const navigateTo = (page, data) => {
    navigation.navigate(page, {
      data: data,
    });
  };

  return (
    <Card style={{backgroundColor: colors.blueDefault, borderRadius: hp(2)}}>
      <View style={{marginVertical: hp(2)}}>
        <View
          style={{
            paddingLeft: wp(5),
            marginBottom: hp(1),
          }}>
          <Text
            style={{
              fontSize: wp(4),
              color: 'white',
              fontFamily: 'Poppins-SemiBold',
            }}>
            Mata Pelajaran
          </Text>
        </View>
        <View>
          <FlatList
            // style={{}}
            // horizontal={true}
            numColumns={3}
            data={dataMap}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => navigateTo('MuridMateriPage', item)}
                key={index}
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginBottom: hp(2),
                }}>
                <Image
                  style={{width: wp(11), height: hp(10), resizeMode: 'contain'}}
                  source={item.img}
                />
                <Text
                  style={{
                    fontSize: wp(3),
                    color: 'white',
                    fontFamily: 'JosefinSans-Regular',
                  }}>
                  {item.label.toUpperCase()}
                </Text>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    </Card>
  );
};

export default PelajaranItem;
