/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import {Container, Header} from 'native-base';
import colors from '../../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Pelajaran from './PelajaranItem';
import Materi from './MateriItem';
import Title from '../../component/Title';
import {ScrollView} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import DefaultHeader from '../../component/DefaultHeader';
import ItemMurid from './itemMurid';
import {useGlobalContext} from '../../context/GlobalContext';

const MuridPage = () => {
  const navigation = useNavigation();
  const {setGlobalState} = useGlobalContext();
  const [, setState] = React.useState({
    isLoading: false,
    userData: {},
  });

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const checkUserData = async () => {
    await updateState({isLoading: true});
    await AsyncStorage.getItem('userData').then((value) => {
      const data = JSON.parse(value);
      if (data !== null) {
        updateState({userData: data});
        setGlobalState({userData: data});
      }
    });
    await updateState({isLoading: false});
  };

  useEffect(() => {
    checkUserData();
  }, []);

  return (
    <Container style={{flex: 1}}>
      <ImageBackground
        source={require('../../res/images/bg2.png')}
        style={{
          flex: 1,
          resizeMode: 'cover',
          // justifyContent: 'center',
          backgroundColor: colors.grayBG,
        }}>
        <DefaultHeader title="Dashboard" dashboardRight />

        {/* Content */}
        <View>
          <ScrollView style={{paddingHorizontal: wp(4), paddingTop: hp(1)}}>
            <View style={{marginVertical: hp(1)}}>
              <Title />
            </View>
            <View style={{marginVertical: hp(1)}}>
              <ItemMurid />
            </View>
            <View style={{marginVertical: hp(0.2)}}>
              <Materi />
            </View>
            <View style={{height: 120}} />
          </ScrollView>
        </View>
      </ImageBackground>
    </Container>
  );
};

export default MuridPage;
