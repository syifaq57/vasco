/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, Image} from 'react-native';
import {Container, View, Text} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const SplashScreen = () => {
  return (
    <Container>
      <View
        style={{
          backgroundColor: 'white',
          flex: 1,
          justifyContent: 'center',
          padding: 20,
        }}>
        <View style={styles.viewPT}>
          <View style={styles.logoView}>
            <Text style={{fontSize: wp('4.5%'), fontWeight: 'bold'}}>
              SplashScreen
            </Text>
          </View>
        </View>
      </View>
    </Container>
  );
};

const styles = StyleSheet.create({
  viewPT: {
    marginHorizontal: wp('5%'),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -15,
  },
  logoView: {
    alignItems: 'center',
  },
  logo: {
    width: hp('20%'),
    height: hp('20%'),
    resizeMode: 'contain',
  },
  loading: {
    width: hp('15%'),
    height: hp('15%'),
    resizeMode: 'contain',
  },
});

export default SplashScreen;
