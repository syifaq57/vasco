/* eslint-disable react-hooks/rules-of-hooks */
import React, {useState} from 'react';
import firestore from '@react-native-firebase/firestore';
import {useGlobalContext} from '../context/GlobalContext';
import storage from '@react-native-firebase/storage';
import uuid from 'react-native-uuid';
import {AsyncStorage} from 'react-native';

export default function Api() {
  const {getGlobalState, setGlobalState, setLoading} = useGlobalContext();
  const materi = firestore().collection('materi');
  const tugas = firestore().collection('tugas');
  const pelajaran = firestore().collection('mata-pelajaran');
  const user = firestore().collection('user');
  const hasiltugas = firestore().collection('hasil-tugas');

  const CreateMateri = async (data, link, doc) => {
    try {
      materi.add({
        judul: data.judul,
        kelas: data.kelas,
        penulis: data.penulis,
        pelajaran: data.pelajaran,
        isi: data.isiMateri,
        lampiran: link ? link : [],
        dokumen: doc ? doc : {},
        createAt: firestore.FieldValue.serverTimestamp(),
      });
    } catch (error) {
      return false;
    }
    return true;
  };

  const inputMateri = async (data, doc) => {
    const url = [];
    if (data.imageObject.length > 0) {
      for (let i = 0; i < data.imageObject.length; i++) {
        const uri = data.imageObject[i].uri;
        const ext = uri.split('.').pop();
        const filename = `${uuid.v1()}.${ext}`;
        const imageRef = storage().ref(`materi/${filename}`);
        await imageRef.putFile(uri);
        url.push({name: filename, uri: await imageRef.getDownloadURL()});
      }
    }
    const solved = CreateMateri(data, url, doc);
    await setLoading(false);
    return solved;
  };

  const EditMateri = (data, link, doc) => {
    try {
      materi.doc(data.id).update({
        judul: data.judul,
        kelas: data.kelas,
        penulis: data.penulis,
        pelajaran: data.pelajaran,
        isi: data.isiMateri,
        lampiran:
          data.lampiran.length > 0
            ? link.concat(data.lampiran)
            : link
            ? link
            : '',
        dokumen: doc ? doc : data.dokumen,
        createAt: firestore.FieldValue.serverTimestamp(),
      });
    } catch (error) {
      return false;
    }
    return true;
  };

  const updateMateri = async (data, doc) => {
    let url = [];
    if (data.imageObject.length > 0) {
      for (let i = 0; i < data.imageObject.length; i++) {
        const uri = data.imageObject[i].uri;
        const ext = uri.split('.').pop();
        const filename = `${uuid.v1()}.${ext}`;
        const imageRef = storage().ref(`materi/${filename}`);
        await imageRef.putFile(uri);
        url.push({name: filename, uri: await imageRef.getDownloadURL()});
      }
    }

    const solved = EditMateri(data, url, doc);
    await setLoading(false);
    return solved;
  };

  const deleteMateri = (data) => {
    firestore().collection('materi').doc(data.id).delete();
    // this.props.reload();
  };

  const CreateTugas = (data, link, doc) => {
    try {
      tugas.add({
        judul: data.judul,
        kelas: data.kelas,
        penulis: data.penulis,
        pelajaran: data.pelajaran,
        isi: data.isi,
        lampiran: link ? link : [],
        dokumen: doc ? doc : {},
        createAt: firestore.FieldValue.serverTimestamp(),
      });
    } catch (error) {
      return false;
    }
    return true;
  };

  const inputTugas = async (data, doc) => {
    console.log('docc', doc);
    const url = [];
    if (data.imageObject.length > 0) {
      for (let i = 0; i < data.imageObject.length; i++) {
        const uri = data.imageObject[i].uri;
        const ext = uri.split('.').pop();
        const filename = `${uuid.v1()}.${ext}`;
        const imageRef = storage().ref(`tugas/${filename}`);
        await imageRef.putFile(uri);
        url.push({name: filename, uri: await imageRef.getDownloadURL()});
      }
    }
    const solved = CreateTugas(data, url, doc);
    await setLoading(false);
    return solved;
  };

  const EditTugas = (data, link, doc) => {
    try {
      tugas.doc(data.id).update({
        judul: data.judul,
        kelas: data.kelas,
        penulis: data.penulis,
        pelajaran: data.pelajaran,
        isi: data.isi,
        lampiran:
          data.lampiran.length > 0
            ? link.concat(data.lampiran)
            : link
            ? link
            : '',
        dokumen: doc ? doc : data.dokumen,
        createAt: firestore.FieldValue.serverTimestamp(),
      });
    } catch (error) {
      return false;
    }
    return true;
  };

  const updateTugas = async (data, doc) => {
    let url = [];
    if (data.imageObject.length > 0) {
      for (let i = 0; i < data.imageObject.length; i++) {
        const uri = data.imageObject[i].uri;
        const ext = uri.split('.').pop();
        const filename = `${uuid.v1()}.${ext}`;
        const imageRef = storage().ref(`tugas/${filename}`);
        await imageRef.putFile(uri);
        url.push({name: filename, uri: await imageRef.getDownloadURL()});
      }
    }
    const solved = EditTugas(data, url, doc);
    await setLoading(false);
    return solved;
  };

  const deleteTugas = (data) => {
    firestore().collection('tugas').doc(data.id).delete();
    // this.props.reload();
  };

  const CreateUser = (data, link) => {
    return new Promise(async (resolve) => {
      user
        .add({
          nama: data.nama,
          kelas: data.kelas,
          username: data.username,
          password: data.password,
          foto: link ? link : '',
          role: data.role,
          createAt: firestore.FieldValue.serverTimestamp(),
        })
        .then(() => {
          resolve(true);
        })
        .catch((e) => {
          resolve(true);
        });
    });
  };

  const inputUser = async (data) => {
    return new Promise(async (resolve) => {
      setLoading(true);
      if (Object.keys(data.imageObject).length > 0) {
        const uri = data.imageObject.uri;
        const ext = uri.split('.').pop();
        const filename = `${uuid.v1()}.${ext}`;
        const imageRef = storage().ref(`user/${filename}`);
        await imageRef.putFile(uri);
        const url = await imageRef.getDownloadURL();
        var solved = await CreateUser(data, url);
        if (solved === true) {
          resolve(true);
        } else {
          resolve(false);
        }
        await setLoading(false);
      } else {
        const solved = await CreateUser(data);
        if (solved === true) {
          resolve(true);
        } else {
          resolve(false);
        }
        await setLoading(false);
      }
    });
  };

  const EditUser = (data, link) => {
    return new Promise(async (resolve) => {
      user
        .doc(data.id)
        .update({
          nama: data.nama,
          kelas: data.kelas,
          username: data.username,
          password: data.password,
          foto: link ? link : data.foto,
          createAt: firestore.FieldValue.serverTimestamp(),
          role: data.role,
        })
        .then(() => {
          resolve(true);
        })
        .catch((e) => {
          resolve(true);
        });
    });
  };

  const updateUser = async (data) => {
    return new Promise(async (resolve) => {
      setLoading(true);
      if (Object.keys(data.imageObject).length > 0) {
        const uri = data.imageObject.uri;
        const ext = uri.split('.').pop();
        const filename = `${uuid.v1()}.${ext}`;
        const imageRef = storage().ref(`user/${filename}`);
        await imageRef.putFile(uri);
        const url = await imageRef.getDownloadURL();
        var solved = await EditUser(data, url);
        if (solved === true) {
          resolve(true);
        } else {
          resolve(false);
        }
        await setLoading(false);
      } else {
        const solved = await EditUser(data);
        if (solved === true) {
          resolve(true);
        } else {
          resolve(false);
        }
        await setLoading(false);
      }
    });
  };

  const deleteUser = (data) => {
    firestore().collection('user').doc(data.id).delete();
    // this.props.reload();
  };

  const CreateHasilTugas = (data, link, doc) => {
    try {
      hasiltugas.add({
        id_tugas: data.id_tugas,
        judul: data.judul,
        penulis: data.penulis,
        isi: data.isi,
        lampiran: link ? link : [],
        dokumen: doc ? doc : {},
        createAt: firestore.FieldValue.serverTimestamp(),
      });
    } catch (error) {
      return false;

    }
    return true;
  };

  const inputHasilTugas = async (data, doc) => {
    const url = [];
    if (data.imageObject.length > 0) {
      for (let i = 0; i < data.imageObject.length; i++) {
        const uri = data.imageObject[i].uri;
        const ext = uri.split('.').pop();
        const filename = `${uuid.v1()}.${ext}`;
        const imageRef = storage().ref(`hasil-tugas/${filename}`);
        await imageRef.putFile(uri);
        url.push({name: filename, uri: await imageRef.getDownloadURL()});
      }
    }
    const solved = CreateHasilTugas(data, url, doc);
    await setLoading(false);
    return solved;
  };

  const EditHasilTugas = (data, link, doc) => {
    try {
      hasiltugas.doc(data.id).update({
        id_tugas: data.id_tugas,
        judul: data.judul,
        penulis: data.penulis,
        isi: data.isi,
        lampiran:
          data.lampiran.length > 0
            ? link.concat(data.lampiran)
            : link
            ? link
            : '',
        dokumen: doc ? doc : data.dokumen,
        createAt: firestore.FieldValue.serverTimestamp(),
      });
    } catch (error) {
      return false;
    }
    return true;
  };

  const updateHasilTugas = async (data, doc) => {
    let url = [];
    if (data.imageObject.length > 0) {
      for (let i = 0; i < data.imageObject.length; i++) {
        const uri = data.imageObject[i].uri;
        const ext = uri.split('.').pop();
        const filename = `${uuid.v1()}.${ext}`;
        const imageRef = storage().ref(`hasil-tugas/${filename}`);
        await imageRef.putFile(uri);
        url.push({name: filename, uri: await imageRef.getDownloadURL()});
      }
    }
    const solved = EditHasilTugas(data, url, doc);
    await setLoading(false);
    return solved;
  };

  const deleteHasilTugas = (data) => {
    firestore().collection('hasil-tugas').doc(data.id).delete();
    // this.props.reload();
  };

  return {
    inputMateri,
    updateMateri,
    deleteMateri,
    materi,

    pelajaran,

    inputTugas,
    updateTugas,
    deleteTugas,
    tugas,

    inputUser,
    updateUser,
    deleteUser,
    user,

    inputHasilTugas,
    updateHasilTugas,
    deleteHasilTugas,
    hasiltugas,
  };
}
