/* eslint-disable react-hooks/rules-of-hooks */
import React from 'react';
import {useNavigation} from '@react-navigation/native';

export const changeNavigate = async () => {
  const navigation = await useNavigation();
  return navigation.goBack();
};
