/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Animated,
  StyleSheet,
} from 'react-native';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import colors from '../res/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ReTextInput = (props) => {
  const inputref = React.useRef(null);

  const initialState = {
    isFieldActive: false,
    position: new Animated.Value(props.value ? 1 : 0),
    secureText: true,
    iconPass: 'eye-slash',
  };
  const [state, setState] = React.useState(initialState);

  const [labelStyle, setLabelStyle] = React.useState({
    position: 'absolute',
    marginLeft: wp(4),
    // marginBottom: hp(1),
    // marginTop: -10,
    fontSize: wp(3.5),
    color: colors.gray10,
    fontFamily: 'Poppins-Regular',
    // backgroundColor: colors.bgErm,
  });

  const updateState = (newData) => {
    setState((prev) => ({
      ...prev,
      ...newData,
    }));
  };
  const titleAnimated = () => {
    return {
      top: state.position.interpolate({
        inputRange: [0, 1],
        outputRange: [hp(2), hp(0.1)],
      }),
      fontSize: state.position.interpolate({
        inputRange: [0, 1],
        outputRange: [wp(4), wp(2.8)],
      }),
      marginLeft: state.position.interpolate({
        inputRange: [0, 1],
        outputRange: [wp(4), wp(2)],
      }),
      color: state.position.interpolate({
        inputRange: [0, 1],
        outputRange: [colors.gray10, colors.gray08],
      }),
    };
  };
  const handleFocus = () => {
    if (!state.isFieldActive) {
      console.log('aktif');

      setState({...state, isFieldActive: true});
      Animated.timing(state.position, {
        toValue: 1,
        duration: 150,
        useNativeDriver: false,
      }).start();
    }
  };

  const handleBlur = () => {
    if (state.isFieldActive && !props.value) {
      // console.log('nonaktif', props.value)

      setState({...state, isFieldActive: false});
      Animated.timing(state.position, {
        toValue: props.value ? 1 : 0,
        duration: 150,
        useNativeDriver: false,
      }).start();
    }
  };

  const handleShowPassword = () => {
    if (state.iconPass === 'eye') {
      updateState({iconPass: 'eye-slash', secureText: true});
    } else {
      updateState({iconPass: 'eye', secureText: false});
    }
  };

  return (
    <View>
      <View
        style={[
          {
            minHeight: hp(7.8),
            borderBottomWidth: 2,
            borderColor: colors.white,
            borderRadius: wp(1.5),
            flexDirection: 'row',
            paddingLeft: wp(3),
            paddingRight: wp(5),
            alignItems: 'center',
            backgroundColor: props.loginInput
              ? 'white'
              : 'rgba(255,255,255, 0.7)',
          },
        ]}>
        <View style={{flex: 1}}>
          <TextInput
            ref={inputref}
            value={props.value}
            // placeholder={props.placeholder}
            maxLength={props.numeric ? 3 : 1000}
            keyboardType={props.numeric ? 'numeric' : 'default'}
            editable={props.editable}
            secureTextEntry={props.passwordInput ? state.secureText : false}
            onFocus={() => {
              handleFocus();
            }}
            onChangeText={(text) => props.onChangeText(text)}
            onBlur={() => {
              handleBlur();
            }}
            style={{
              fontSize: wp(4),
              paddingTop: hp(2),
              marginBottom: hp(-0.6),
              fontFamily: 'Poppins-Regular',
              color: colors.gray08,
              // borderWidth: 1,
            }}
          />
        </View>
        {props.passwordInput ? (
          <View style={{}}>
            <TouchableOpacity onPress={() => handleShowPassword()}>
              <FontAweSome
                name={state.iconPass}
                color={colors.blueErm}
                size={wp(5)}
              />
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
      <Animated.Text
        onPress={() => inputref.current.focus()}
        style={[labelStyle, titleAnimated()]}>
        {' '}
        {props.label}{' '}
      </Animated.Text>
    </View>
  );
};

export default ReTextInput;
