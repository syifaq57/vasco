/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Picker} from 'native-base';
import colors from '../res/colors';
import Modal from 'react-native-modal';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ScrollView} from 'react-native-gesture-handler';
import FontAweSome from 'react-native-vector-icons/FontAwesome';

const RePicker = (props) => {
  const [selectedValue, setSelectedValue] = useState(props.value);
  const [showList, setShowList] = useState(false);

  const toggleShowList = () => {
    setShowList(!showList);
  };
  return (
    <View style={{}}>
      <TouchableOpacity
        style={{
          borderWidth: 2,
          borderColor: colors.blueDefault,
          minHeight: hp(7.8),
          borderRadius: wp(1.5),
          flexDirection: 'row',
          paddingLeft: wp(3),
          paddingRight: wp(5),
          alignItems: 'center',
          backgroundColor: props.backgroundColor
            ? props.backgroundColor
            : 'rgba(255,255,255, 0.7)',
        }}
        onPress={() => toggleShowList()}>
        <Text
          style={{
            position: 'absolute',
            top: selectedValue.length > 0 ? hp(0.1) : hp(2),
            marginLeft: selectedValue.length > 0 ? wp(2.4) : wp(4.4),
            fontSize: selectedValue.length > 0 ? wp(2.8) : wp(4),
            fontFamily: 'Poppins-Regular',
            color:
              selectedValue.length > 0
                ? colors.gray08
                : props.textColor
                ? props.textColor
                : colors.gray10,
          }}>
          {props.label}
        </Text>
        {selectedValue.length > 0 ? (
          <View
            style={{
              paddingLeft: wp(1),
              marginTop: hp(1.3),
              marginBottom: hp(-0.6),
            }}>
            <Text
              style={{
                fontSize: wp(4),
                fontFamily: 'Poppins-Regular',
                color: props.textColor ? props.textColor : colors.gray08,
              }}>
              {selectedValue}
            </Text>
          </View>
        ) : null}
        <View style={{position: 'absolute', right: wp(4)}}>
          <FontAweSome
            name="chevron-down"
            color={props.textColor ? props.textColor : colors.gray10}
            size={wp(4)}
          />
        </View>
      </TouchableOpacity>

      <Modal
        animationIn={'slideInDown'}
        isVisible={showList}
        onBackdropPress={() => toggleShowList()}
        backdropOpacity={0.5}
        animationOut={'slideOutUp'}
        animationInTiming={1000}>
        <View
          style={{
            // position: 'absolute',
            maxHeight: hp(45),
            // borderRadius: wp(2),
          }}>
          <View
            style={{
              borderTopLeftRadius: wp(1),
              borderTopRightRadius: wp(1),
              padding: wp(3),
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: colors.blueDefault,
            }}>
            <Text
              style={{
                fontFamily: 'JosefinSans-Bold',
                fontSize: wp(5),
                color: 'white',
              }}>
              {props.label}
            </Text>
          </View>
          <ScrollView style={{backgroundColor: 'white'}}>
            {props.data.map((data, index) => (
              <TouchableOpacity
                onPress={() => {
                  setSelectedValue(data.value);
                  props.onValueChange(data.value);
                  toggleShowList();
                }}
                key={index}
                style={{
                  backgroundColor:
                    index % 2 === 0 ? colors.soft5 : colors.soft4,
                  // borderRadius: wp(2),

                  paddingVertical: hp(2),
                  paddingHorizontal: wp(3),
                  // borderBottomWidth: 0.5,
                  flexDirection: 'row',
                }}>
                <View style={{justifyContent: 'center', marginRight: wp(2)}}>
                  <FontAweSome
                    name="chain"
                    color={colors.gray10}
                    size={wp(3)}
                  />
                </View>
                <View>
                  <Text
                    style={{
                      fontSize: hp(2.8),
                      fontFamily: 'JosefinSans-Bold',
                      color: colors.gray10,
                    }}>
                    {data.label}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      </Modal>
    </View>
  );
};

export default RePicker;
