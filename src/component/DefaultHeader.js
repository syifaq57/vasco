/* eslint-disable react-native/no-inline-styles */
import React, {useRef, useState} from 'react';
import {
  View,
  Animated,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Header} from 'native-base';
import colors from '../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';

const FadeInView = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current; // Initial value for opacity: 0

  React.useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 350,
      useNativeDriver: false,
    }).start();
  }, [fadeAnim]);

  return (
    <Animated.View // Special animatable View
      style={{
        ...props.style,
        opacity: fadeAnim, // Bind opacity to animated value
      }}>
      {props.children}
    </Animated.View>
  );
};

const initialState = {
  searchVisible: false,
  headerVisible: true,
  search: '',
};

const DefaultHeader = (props) => {
  const [state, setState] = useState(initialState);
  const navigation = useNavigation();

  const getState = () => {
    return state;
  };

  const updateState = (newData) => {
    setState((prev) => ({
      ...prev,
      ...newData,
    }));
  };

  return (
    <Header
      transparent
      style={{
        borderBottomWidth: 0,
        backgroundColor: colors.blueDefault,
        paddingLeft: 0,
        paddingRight: 0,
        // position: 'absolute',
      }}
      androidStatusBarColor={colors.blueDefault}>
      {getState().searchVisible ? (
        <FadeInView
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: wp(4),
            backgroundColor: 'white',
          }}>
          <TouchableOpacity
            style={{marginRight: wp(2), flex: 0.5, justifyContent: 'center'}}
            onPress={() => {
              updateState({searchVisible: false});
              updateState({headerVisible: true});
            }}>
            <FontAweSome
              name="close"
              color={colors.blueDefault}
              size={hp(3.5)}
            />
          </TouchableOpacity>
          <View style={{flex: 6}}>
            <TextInput
              onChangeText={(val) => {
                props.onSearchChange(val);
              }}
              style={{fontSize: wp(4.5)}}
              placeholder="Search"
            />
          </View>
        </FadeInView>
      ) : (
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: wp(4),
            // backgroundColor: 'white',
          }}>
          {props.backButton ? (
            <View style={{flex: 0.7}}>
              <TouchableOpacity
                onPress={() => {
                  navigation.goBack();
                }}>
                <FontAweSome name="arrow-left" color="white" size={hp(3)} />
              </TouchableOpacity>
            </View>
          ) : null}
          <View style={{flex: 5}}>
            <Text
              // uppercase={false}
              style={{
                fontSize: hp(3.5),
                fontFamily: 'JosefinSans-Bold',
                color: 'white',
              }}>
              {props.title}
            </Text>
          </View>
          {props.searchable ? (
            <View style={{flex: 1, alignItems: 'center', paddingTop: 7}}>
              <TouchableOpacity
                onPress={() => {
                  updateState({searchVisible: true});
                  updateState({headerVisible: false});
                }}>
                <FontAweSome name="search" color="white" size={hp(3.5)} />
              </TouchableOpacity>
            </View>
          ) : null}
          {props.rightButton ? (
            <View style={{flex: 1, alignItems: 'center', paddingTop: 7}}>
              <TouchableOpacity
                onPress={() => {
                  props.onPressButtonRight();
                }}>
                <FontAweSome
                  name={props.rightButton}
                  color="white"
                  size={hp(3.5)}
                />
              </TouchableOpacity>
            </View>
          ) : null}
          {props.dashboardRight ? (
            <View style={{flex: 1, alignItems: 'center', paddingTop: 7}}>
              <TouchableOpacity
                style={{
                  borderRadius: wp(50),
                }}>
                <Image
                  style={{
                    // marginTop: hp(2),
                    borderRadius: wp(50),

                    height: hp(5),
                    width: wp(8),
                    resizeMode: 'cover',
                  }}
                  source={require('../res/images/user.jpg')}
                />
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
      )}
    </Header>
  );
};

export default DefaultHeader;
