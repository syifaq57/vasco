/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import colors from '../res/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ReButtonSave = (props) => {
  return (
    <TouchableOpacity
      disabled={props.disabled}
      style={{
        marginVertical: wp(2),
        backgroundColor: props.color
          ? props.color
          : props.disabled
          ? colors.gray03
          : colors.soft1,
        borderRadius: wp(1.5),
        height: hp(7.6),
        justifyContent: 'center',
        alignItems: 'center',
      }}
      onPress={props.onPress}>
      <Text
        style={{
          fontSize: wp(4.5),
          color: colors.white,
          // fontWeight: 'bold',
          fontFamily: 'JosefinSans-Bold',
        }}>
        {props.title ? props.title : 'SIMPAN'}
      </Text>
    </TouchableOpacity>
  );
};

export default ReButtonSave;
