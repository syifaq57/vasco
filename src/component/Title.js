/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, Image} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../res/colors/index';

const Title = (props) => {
  return (
    <View
      style={{
        borderRadius: wp(2),
        padding: wp(2),
        paddingLeft: wp(6),
        flexDirection: 'row',
        paddingHorizontal: wp(4),
        alignItems: 'center',
        backgroundColor: props.backgroundColor
          ? props.backgroundColor
          : 'rgba(255, 255, 255, 0.8)',
        shadowColor: '#000',
        shadowOffset: {
          width: 5,
          height: 20,
        },
        shadowOpacity: 0.1,
        shadowRadius: 1.0,
        elevation: 2,
      }}>
      <View style={{flex: 1}}>
        <Image
          style={{
            // marginTop: hp(2),
            height: hp(7),
            // borderRadius: wp(50),
            width: wp(13),
            resizeMode: 'contain',
          }}
          source={require('../res/images/school.png')}
        />
      </View>
      <View style={{flex: 4, paddingHorizontal: wp(2), paddingVertical: hp(1)}}>
        <View>
          <Text
            ellipsizeMode="tail"
            numberOfLines={1}
            View
            style={{
              fontSize: hp(3),
              color: props.backgroundColor ? colors.white : colors.blueDefault,
              fontFamily: 'JosefinSans-Bold',
            }}>
            Vasco Study
          </Text>
        </View>
        <View style={{marginLeft: wp(1)}}>
          <Text
            style={{
              fontSize: hp(2),
              fontFamily: 'TimesNRCyrMT',
            }}>
            SMP YPK Merauke
          </Text>
        </View>
      </View>
    </View>
  );
};

export default Title;
