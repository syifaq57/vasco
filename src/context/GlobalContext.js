import React, {useState, useContext} from 'react';
import ReLoadingModal from '../component/ReLoadingModal';

/* ===========> BACA DULU SEBELUM EDIT <===========

TOLONG JANGAN MASUKKAN RANDOM VARIABLE KE SINI, ATAU KONSULTASIKAN KE MAINTAINER BO

===========> BACA DULU SEBELUM EDIT <=========== */

export const GlobalContext = React.createContext([{}, () => {}]);

const initialSnackBar = {
  show: false,
  type: '', // error, success, warning
  message: '',
};

const GlobalProvider = ({children}) => {
  const [state, setState] = useState({
    snackbar: initialSnackBar,
    userData: {},
    // context right Filter
    globalLoading: false,
  });

  const setGlobalState = (newData) => {
    setState((prev) => ({
      ...prev,
      ...newData,
    }));
  };

  const getGlobalState = (key) => {
    if (key) {
      return state[key];
    }
    return state;
  };

  const setLoading = (load) => {
    setGlobalState({globalLoading: load});
  };

  return (
    <GlobalContext.Provider
      value={{
        getGlobalState,
        setGlobalState,
        setLoading,
      }}>
      {children}
      <ReLoadingModal />
    </GlobalContext.Provider>
  );
};

export const useGlobalContext = () => {
  const value = useContext(GlobalContext);
  if (value == null) {
    throw new Error('useGlobalContext() called outside of a GlobalProvider?');
  }
  return value;
};

export default GlobalProvider;
